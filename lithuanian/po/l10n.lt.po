#
msgid ""
msgstr ""
"Project-Id-Version: Debian webwml organization\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: (null)\n"
"PO-Revision-Date: 2009-02-22 14:54+0300\n"
"Last-Translator: Aleksandr Charkov <alcharkov@gmail.com>\n"
"Language-Team: unknown\n"
"Language: \n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"

#: ../../english/international/l10n/dtc.def:10
msgid "File"
msgstr "Failas"

#: ../../english/international/l10n/dtc.def:14
msgid "Package"
msgstr "Paketas"

#: ../../english/international/l10n/dtc.def:18
msgid "Score"
msgstr "Rezultatas"

#: ../../english/international/l10n/dtc.def:22
msgid "Translator"
msgstr "Vertėjas"

#: ../../english/international/l10n/dtc.def:26
msgid "Team"
msgstr "Komanda"

#: ../../english/international/l10n/dtc.def:30
msgid "Date"
msgstr "Data"

#: ../../english/international/l10n/dtc.def:34
msgid "Status"
msgstr "Statusas"

#: ../../english/international/l10n/dtc.def:38
msgid "Strings"
msgstr "Eilutė"

#: ../../english/international/l10n/dtc.def:42
msgid "Bug"
msgstr "Klaida"

#: ../../english/international/l10n/dtc.def:49
msgid "<get-var lang />, as spoken in <get-var country />"
msgstr "<get-var lang />, kuria kalbama <get-var country />"

#: ../../english/international/l10n/dtc.def:54
msgid "Unknown language"
msgstr "Nežinoma kalba"

#: ../../english/international/l10n/dtc.def:64
msgid "This page was generated with data collected on: <get-var date />."
msgstr "Šis puslapis yra sukurtas naudojant duomenis: <get-var date />."

#: ../../english/international/l10n/dtc.def:69
msgid "Before working on these files, make sure they are up to date!"
msgstr "Prieš dirbant su šitas failais, įsitikinkite kad jie yra aktualūs!"

#: ../../english/international/l10n/dtc.def:79
msgid "Section: <get-var name />"
msgstr "Skyrius: <get-var name />"

#: ../../english/international/l10n/menu.def:10
msgid "L10n"
msgstr "L10n"

#: ../../english/international/l10n/menu.def:14
msgid "Language list"
msgstr "Kalbų sąrašas"

#: ../../english/international/l10n/menu.def:18
msgid "Ranking"
msgstr "Palyginimas"

#: ../../english/international/l10n/menu.def:22
msgid "Hints"
msgstr "Patarimai"

#: ../../english/international/l10n/menu.def:26
msgid "Errors"
msgstr "Klaidos"

#: ../../english/international/l10n/menu.def:30
msgid "POT files"
msgstr "POT failai"

#: ../../english/international/l10n/menu.def:34
msgid "Hints for translators"
msgstr "Patarimai vertėjams"
