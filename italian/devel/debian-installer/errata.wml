#use wml::debian::template title="Errata corrige dell'Installatore Debian"
#use wml::debian::recent_list
#use wml::debian::translation-check translation="548e836a27bec221897b3d647cb4f4f338c565a4" maintainer="Luca Monducci" mindelta="1" maxdelta="1"
#include "$(ENGLISHDIR)/devel/debian-installer/images.data"


<h1>Errata per <humanversion /></h1>


<p>
Questo è l'elenco dei problemi conosciuti presenti nella versione <humanversion />
dell'Installatore Debian. Chi riscontra un problema non ancora presente
in questa pagina è invitato a inviare un <a href="$(HOME)/releases/stable/amd64/ch05s04#submit-bug">resoconto
dell'installazione</a> in cui viene descritto il problema.
</p>


<dl class="gloss">
	<dt>La modalità di ripristino non funziona nell'installatore grafico</dt>
	<dd>Durante i test dell'immagine Bullseye RC 1 è stato scoperto
	che la modalità di ripristino sembra non funzionare (<a
	href="https://bugs.debian.org/987377">#987377</a>).
	Inoltre, l'etichetta <q>Rescue</q> nella prima schermata deve
	essere adeguata al tema grafico usato per Bullseye.
	<br />
	<b>Stato:</b> Risolto in Bullseye RC 2.</dd>

	<dt>Molte schede grafiche AMD richiedono il firmware amdgpu</dt>
	<dd>Sembra che sia in crescita la necessità di installare il firmware
	amdgpu (tramite il pacchetto non-free <code>firmware-amd-graphics</code>)
	per evitare che lo schermo resti nero durante l'avvio del sistema
	installato. Al momento del rilascio di Bullseye RC 1, persino usando
	un'immagine d'installazione che include tutti i pacchetti con firmware,
	l'installatore non rileva la necessità di quel componente specifico.
	Consultare la <a href="https://bugs.debian.org/989863">segnalazione
	di bug</a> per seguire le attività.
	<br />
	<b>Stato:</b> Corretto in Bullseye RC 3.</dd>

	<dt>L'installazione desktop potrebbe non funzionare con il solo CD#1</dt>
	<dd>Poiché lo spazio disponibile sul primo CD è limitato, non tutti i
	pacchetti necessari per l'ambiente desktop GNOME sono presenti nel CD.
	Per completare l'installazione è necessario usare anche altri
	supporti (per esempio un secondo CD oppure un mirror in rete).<br />
	<b>Stato:</b> È improbabile che il lavoro per far entrare altri
	pacchetti nel CD#1 continui.</dd>

	<dt>LUKS2 è incompatibile con il supporto cryptodisk di GRUB</dt>
	<dd>È da poco stato scoperto che GRUB non supporta LUKS2. Ciò comporta
	che gli utenti che vogliono usare <tt>GRUB_ENABLE_CRYPTODISK</tt>
	devono inevitabilmente avere una direcotry <tt>/boot</tt> separata e
	non criptata (<a href="https://bugs.debian.org/927165">#927165</a>).
	Questa configurazione non è supportata dall'installatore ma merita
	comunque di esssere documentata in modo più prominente ed è opportuno
	permettere almeno la possibilità di scegliere LUKS1 durante il processo
	d'installazione.
	<br />
	<b>Stato:</b> Sono state raccolte alcune idee riguardo a questo bug;
	i manutentori di cryptsetup hanno praparato della <a
	href="https://cryptsetup-team.pages.debian.net/cryptsetup/encrypted-boot.html">documentazione
	specifica</a>.</dd>

</dl>
