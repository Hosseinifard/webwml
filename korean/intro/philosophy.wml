#use wml::debian::template title="데비안 철학: 왜 하는가 그리고 어떻게 하는가"
#include "$(ENGLISHDIR)/releases/info"
#use wml::debian::translation-check translation="8d625100deb403dba223e3cecbac44a283fe02ff" maintainer="Sebul" 

# translators: some text is taken from /intro/about.wml

<ul class="toc">
<li><a href="#what">데비안은 무엇인가요?</a>
<li><a href="#free">모두 무료인가요?</a>
<li><a href="#how">커뮤니티는 프로젝트에서 어떻게 일하나요?</a>
<li><a href="#history">모든 것은 어떻게 시작되었나요?</a>
</ul>

<h2><a name="what">데비안은 무엇인가요?</a></h2>

<p><a href="$(HOME)/">데비안 프로젝트</a>는 <a href="free">자유</a>
운영체제를 만들려는 개인의 모임입니다.
우리가 만든 이 운영체제를 <strong>데비안</strong>이라 부릅니다.

<p>운영체제는 컴퓨터를 실행하도록 하는 기본 프로그램 및 유틸리티의 집합입니다.
운영체제의 핵심에는 커널이 있습니다.
커널은 컴퓨터에서 가장 기본적인 프로그램이며 모든 기본 관리 작업을 수행하고 다른 프로그램을 시작할 수 있습니다.

<p>데비안 시스템은 현재 <a href="https://www.kernel.org/">리눅스</a>
커널 또는 <a href="https://www.freebsd.org/">FreeBSD</a> 커널을 씁니다.
리눅스는 <a href="https://en.wikipedia.org/wiki/Linus_Torvalds">리누스 토발즈</a>가 시작하여 
전 세계 수천 명의 프로그래머가 지원하는 소프트웨어입니다. 
FreeBSD는 커널 및 다른 소프트웨어를 포함하는 운영체제입니다.

<p>그러나, 데비안을 다른 커널 주로 <a href="https://www.gnu.org/software/hurd/hurd.html">Hurd</a>를 위한 작업이 진행중입니다.
Hurd는 다른 기능을 구현하기 위해 마이크로 커널(예: Mach)에서 실행되는 서버 모음입니다. 
Hurd는 <a href="https://www.gnu.org/">GNU 프로젝트</a>에서 만든 자유 소프트웨어입니다.

<p>운영체제를 채우는 기본 도구 대부분은 <a href="https://www.gnu.org/">GNU 프로젝트</a>에서 가져온 것이며, 
따라서 이름이 GNU/Linux, GNU/kFreeBSD 및 GNU/Hurd 입니다. 
이 도구도 무료입니다.

<p>물론 사람들이 원하는 것은 애플리케이션 소프트웨어입니다. 
문서 편집에서 사업 운영, 게임, 더 많은 소프트웨어 작성에 이르기까지 원하는 것을 얻을 수 있는 프로그램입니다. 
데비안은 <packages_in_stable>개 넘는 <a href="$(DISTRIB)/packages">패키지</a>
(컴퓨터에서 쉽게 설치할 수 있도록 적절한 형식으로 번들로 제공되는 사전 컴파일된 소프트웨어), 
패키지 관리자(APT) 및 기타 유틸리티를 통해 단일 애플리케이션을 설치하는 것처럼 수천 대의 컴퓨터에서 수천 개의 패키지를 쉽게 관리할 수 있습니다. 
모두 <a href="free">free</a>입니다.
</p>

<p>그것은 약간 탑과 같습니다. 
기본에는 커널이 있습니다. 
그 위에 모든 기본 도구가 있습니다. 
다음은 컴퓨터에서 실행하는 모든 소프트웨어입니다. 
탑 꼭대기에는 데비안이 있습니다. &mdash;
모든 것을 세심하게 정리하고 장착하여 모든 것이 함께 작동하도록 합니다.

<h2>모두 <a href="free" name="free">free 인가요?</a></h2>

<p>단어 "free"를 쓸 때, 소프트웨어 <strong>freedom</strong>을 뜻합니다. 
더 많은 것을 
<a href="free">"free 소프트웨어"의 뜻</a> 그리고 <a href="https://www.gnu.org/philosophy/free-sw">자유소프트웨어재단이 
 말하는 것</a>에서 그 주제에 대해 볼 수 있습니다.

<p>왜 사람들이 소프트웨어를 만들고 신중하게 패키징한 다음 모든 것을 <em>제공</em>하는 데 시간을 쓰나요? 
답변은 기여하는 사람들만큼 다양합니다. 
어떤 사람들은 다른 사람들을 돕고 싶어합니다. 
많은 사람들이 컴퓨터에 대해 더 많이 배우기 위해 프로그램을 만듭니다. 
점점 더 많은 사람들이 소프트웨어의 과장된 가격을 피할 수 있는 방법을 찾고 있습니다. 
점점 더 많은 사람들이 다른 사람들로부터 받은 모든 훌륭한 무료 소프트웨어에 대한 감사의 표시로 기여하고 있습니다. 
많은 학계에서 연구 결과를 더 널리 사용하기 위해 자유 소프트웨어를 만듭니다. 
기업은 자유 소프트웨어를 유지 관리하여 개발 방법에 대해 말할 수 있습니다. 
새로운 기능을 직접 구현하는 것보다 더 빠른 방법은 없습니다! 
물론, 우리 중 많은 사람들이 단지 그것을 아주 재미있게 생각합니다.

<p>데비안은 자유 소프트웨어에 매우 헌신적이어서 
그 약속이 서면 문서로 공식화된다면 유용할 것이라고 생각했습니다. 
그래서 <a href="$(HOME)/social_contract">사회 계약</a>이 나왔습니다.

<p>데비안은 자유 소프트웨어를 믿지만, 
사람들이 자신의 컴퓨터에 자유 소프트웨어가 아닌 소프트웨어를 설치하기를 바라거나 필요한 경우가 있습니다. 
가능하면 데비안이 이를 지원할 것입니다. 
데비안 시스템에 자유 소프트웨어가 아닌 소프트웨어를 설치하는 것이 유일한 작업인 패키지도 늘고 있습니다.

<h2><a name="how">커뮤니티는 프로젝트에서 어떻게 일하나요?</a></h2>

<p>데비안은 여유 시간에 자원봉사를 하는 <a href="$(DEVEL)/developers.loc">전 세계에 퍼져있는</a> 
거의 천 명의 활발한 개발자들이 만듭니다. 
실제로 직접 만난 개발자는 거의 없습니다. 
소통은 주로 이메일(lists.debian.org의 메일링리스트)과 
IRC(irc.debian.org의 #debian 채널)를 통해 이루어집니다.
</p>

<p>데비안 프로젝트는 신중하게 <a href="organization">조직된 구조</a>를 가집니다. 
내부에서 데비안이 어떻게 보이는지에 대한 자세한 정보는 <a href="$(DEVEL)/">개발자 코너</a>를 보십시오 .
</p>

<p>커뮤니티 동작을 설명하는 주요 문서:
<ul>
<li><a href="$(DEVEL)/constitution">데비안 헌법</a></li>
<li><a href="../social_contract">사회 계약 및 자유 소프트웨어 가이드라인</a></li>
<li><a href="diversity">다양성 선언문</a></li>
<li><a href="../code_of_conduct">행동강령</a></li>
<li><a href="../doc/developers-reference/">개발자 참조</a></li>
<li><a href="../doc/debian-policy/">데비안 정책</a></li>
</ul>

<h2><a name="history">모든 것은 어떻게 시작되었나요?</a></h2>

<p>데비안은 1993년 8월 이안 머독에 의해 리눅스와 GNU의 정신으로 공개적으로 만들어질 새로운 배포판으로 시작되었습니다. 
데비안은 신중하고 양심적으로 합쳐지고, 비슷한 보살핌으로 유지되고 지지받도록 되어 있었습니다. 
이 조직은 소규모의 긴밀한 자유 소프트웨어 해커 그룹으로 시작했으며 
점차 개발자와 사용자들로 구성된 대규모 조직화된 커뮤니티로 성장했습니다.
<a href="$(DOC)/manuals/project-history/">데비안 역사</a>를 보세요.

<p>여러 사람이 물어서 대답하는데, 데비안은 /&#712;de.bi.&#601;n/ 으로 발음합니다.
데비안 창시자 Ian Murdock, 그리고 그 아내 Debra에서 왔습니다.
