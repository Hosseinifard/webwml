#use wml::debian::template title="Debian GNU/Hurd -- Notícias" NOHEADER="yes"
#include "$(ENGLISHDIR)/ports/hurd/menu.inc"
#use wml::debian::translation-check translation="b0a09586f30abf1c4b51d0b5308133e2b24170f6"

<h1>Notícias sobre o Debian GNU/Hurd</h1>

<h3><:=spokendate ("2019-07-07"):></h3>

<p>Debian GNU/Hurd 2019 <em>lançado</em>!</p>

<p>É com imenso prazer que o time do Debian GNU/Hurd anuncia o
<strong>lançamento do Debian GNU/Hurd 2019</strong>. <br />
Este é um snapshot do Debian "sid" na época do lançamento da versão estável
Debian "buster" (julho de 2019), então ele é majoritariamente
baseado nos mesmos fontes. Ele <em>não</em> é uma
versão oficial do Debian, mas é um lançamento oficial do porte Debian
GNU/Hurd.</p>

<p>As imagens ISO de instalação podem ser baixadas do site
<a href="https://cdimage.debian.org/cdimage/ports/10.0/hurd-i386/">cdimage</a>
nos três "sabores" usuais do Debian: NETINST, CD ou DVD. Além do amigável
instalador do Debian, uma imagem de disco pré-instalada também está disponível,
proporcionando uma maneira ainda mais fácil de testar o Debian GNU/Hurd. O
modo mais fácil de executá-la é
<a href="https://www.debian.org/ports/hurd/hurd-install">dentro de uma VM como o qemu</a></p>

<p>O Debian GNU/Hurd atualmente está disponível para a arquitetura i386 e com
80% do repositório do Debian, e mais está por vir!</p>

<ul>
<li>Um tradutor ACPI está disponível, atualmente é somente usado para desligar
o sistema.</li>
<li>A pilha LwIP TCP/IP agora está disponível como uma opção.</li>
<li>Uma intermediação PCI foi introduzida e será útil para gerenciar
adequadamente o acesso ao PCI, bem como para fornecer acesso detalhado ao
hardware.</li>
<li>Suporte para LLVM foi introduzido.</li>
<li>Novas otimizações incluem carregamentos de dados protegidos, melhor
administração de paginação e expedição de mensagens, e sincronização gsync.</li>
</ul>

<p>Por favor, certifique-se de ler as
<a href="https://www.debian.org/ports/hurd/hurd-install">informações de configuração</a>,
o <a href="https://www.gnu.org/software/hurd/faq.html">FAQ</a>
(ou <a href="http://darnassus.sceen.net/~hurd-web/faq/">sua última versão</a>),
e a <a href="https://www.gnu.org/software/hurd/hurd/documentation/translator_primer.html">cartilha sobre tradutores</a>
para ter uma ideia das excelentes funcionalidades do GNU/Hurd.</p>

<p>Gostaríamos de agradecer a todas as pessoas que trabalharam no GNU/Hurd
<a href=https://www.gnu.org/software/hurd/history.html>no passado</a>.
Nunca tivemos muitas pessoas trabalhando (e ainda não temos hoje, por favor,
<a href="https://www.gnu.org/software/hurd/contributing.html">participe</a>!),
mas no fim das contas muitas pessoas contribuíram de uma forma ou de outra.
<strong>Obrigado(a), pessoal!</strong></p>

<h3><:=spokendate ("2017-06-18"):></h3>

<p>Debian GNU/Hurd 2017 <em>lançado</em>!</p>

<p>É com imenso prazer que o time do Debian GNU/Hurd anuncia o
<strong>lançamento do Debian GNU/Hurd 2017</strong>. <br />
Este é um snapshot do Debian "sid" na época do lançamento da versão estável
Debian "stretch" (maio de 2017), então ele é majoritariamente
baseado nos mesmos fontes. Ele <em>não</em> é uma
versão oficial do Debian, mas é um lançamento do porte oficial Debian GNU/Hurd.</p>

<p>As imagens ISO de instalação podem ser baixadas do site
<a href="https://cdimage.debian.org/cdimage/ports/9.0/hurd-i386/">cdimage</a>
nos três "sabores" usuais do Debian: NETINST, CD ou DVD. Além do amigável
instalador do Debian, uma imagem de disco pré-instalada também está disponível,
proporcionando uma maneira ainda mais fácil de testar o Debian GNU/Hurd. O
modo mais fácil de executá-la é
<a href="https://www.debian.org/ports/hurd/hurd-install">dentro de uma VM como o qemu</a></p>

<p>O Debian GNU/Hurd atualmente está disponível para a arquitetura i386 e com
80% do repositório do Debian, e mais está por vir!</p>

<ul>
<li>Os pacotes centrais do GNU Hurd e GNU Mach foram atualizados para as
   versões 0.9 e 1.8, respectivamente. Além de outras inúmeras melhorias, eles
   trazem uma vasta melhora na estabilidade a partir do carregamento de memória
   e uptime prolongado.</li>
<li>A ferramenta nativa fakeroot foi muito melhorada, permitindo ser usada para
   construir pacotes, o que tornou-se mais rápido e seguro.</li>
<li>Agora é possível executar subhurds como usuário(a) sem privilégios, desse
modo fornecendo virtualização fácil e leve.</li>
<li>O tamanho de memória suportada foi estendida para mais de 3GiB.</li>
</ul>

<p>Por favor, certifique-se de ler as
<a href="https://www.debian.org/ports/hurd/hurd-install">informações de configuração</a>,
o <a href="https://www.gnu.org/software/hurd/faq.html">FAQ</a>
(ou <a href="http://darnassus.sceen.net/~hurd-web/faq/">sua última versão</a>),
e a <a href="https://www.gnu.org/software/hurd/hurd/documentation/translator_primer.html">cartilha sobre tradutores</a>
para ter uma ideia das excelentes funcionalidades do GNU/Hurd.</p>

<p>NGostaríamos de agradecer a todas as pessoas que trabalharam no GNU/Hurd
<a href=https://www.gnu.org/software/hurd/history.html>no passado</a>.
Nunca tivemos muitas pessoas trabalhando (e ainda não temos hoje, por favor,
<a href="https://www.gnu.org/software/hurd/contributing.html">participe</a>!),
mas no fim das contas muitas pessoas contribuíram de uma forma ou de outra.
<strong>Obrigado(a), pessoal!</strong></p>


<h3><:=spokendate ("2015-04-25"):></h3>

<p>Debian GNU/Hurd 2015 <em>lançado</em>!</p>

<p>É com imenso prazer que o time do Debian GNU/Hurd anuncia o
<strong>lançamento do Debian GNU/Hurd 2015</strong>. <br />
Este é um snapshot do Debian "sid" na época do lançamento da versão estável
Debian "jessie" (abril de 2015), então ele é majoritariamente
baseado nos mesmos fontes. Ele <em>não</em> é uma
versão oficial do Debian, mas é um lançamento do porte oficial Debian GNU/Hurd.</p>

<p>As imagens ISO de instalação podem ser baixadas do site
<a href="https://cdimage.debian.org/cdimage/ports/8.0/hurd-i386/">portes do Debian</a>
nos três "sabores" usuais do Debian: NETINST, CD ou DVD. Além do amigável
instalador do Debian, uma imagem de disco pré-instalada também está disponível,
proporcionando uma maneira ainda mais fácil de testar o Debian GNU/Hurd. O
modo mais fácil de executá-la é
<a href="https://www.debian.org/ports/hurd/hurd-install">dentro de uma VM como o qemu</a></p>

<p>O Debian GNU/Hurd atualmente está disponível para a arquitetura i386 e com
80% do repositório do Debian, e mais está por vir!</p>

<p>Desde o último lançamento do snapshot que coincidiu com o "wheezy", o sistema
init foi trocado por sysvinit para uma experiência mais próxima à do Debian.
Alterações adicionais desde o último snapshot incluem:</p>

<ul>
<li>Os pacotes centrais do GNU Hurd e GNU Mach foram atualizados para as versões
   0.6 e 1.5, respectivamente. Além de outras inúmeras melhorias, eles
   trazem uma vasta melhora na estabilidade a partir do carregamento de memória
   e uptime prolongado.</li>
<li>Os drivers de rede foram migrados para drivers de espaço de usuário(a)
   usando a infraestrutura NetDDE e um código-base do Linux-2.6.32.</li>
</ul>

<p>
Pacotes novos e atualizados que devem ser observados, porque demandaram
esforços consideráveis de portabilidade e/ou são conhecidos por funcionaram
bem no Debian GNU/Hurd, incluem Iceweasel 31 ESR, Xfce 4.10,
X.org 7.7 e Emacs 24.4.
</p>

<p>Por favor, certifique-se de ler as
<a href="https://www.debian.org/ports/hurd/hurd-install">informações de configuração</a>,
o <a href="https://www.gnu.org/software/hurd/faq.html">FAQ</a>
(ou <a href="http://darnassus.sceen.net/~hurd-web/faq/">sua última versão</a>),
e a <a href="https://www.gnu.org/software/hurd/hurd/documentation/translator_primer.html">cartilha sobre tradutores</a>
para ter uma ideia das excelentes funcionalidades do GNU/Hurd.</p>

<p>Gostaríamos de agradecer a todas as pessoas que trabalharam no GNU/Hurd
<a href=https://www.gnu.org/software/hurd/history.html>no passado</a>.
Nunca tivemos muitas pessoas trabalhando (e ainda não temos hoje, por favor,
<a href="https://www.gnu.org/software/hurd/contributing.html">participe</a>!),
mas no fim das contas muitas pessoas contribuíram de uma forma ou de outra.
<strong>Obrigado(a), pessoal!</strong></p>


<h3><:=spokendate ("2013-05-21"):></h3>

<p>Debian GNU/Hurd 2013 <em>lançado</em>!</p>

<p>É com imenso prazer que o time do Debian GNU/Hurd anuncia o
<strong>lançamento do Debian GNU/Hurd 2013</strong>. <br />
Este é um snapshot do Debian "sid" na época do lançamento da versão estável
Debian "wheezy" (maio de 2013), então ele é majoritariamente
baseado nos mesmos fontes. Ele <em>não</em> é uma
versão oficial do Debian, mas é um lançamento do porte oficial Debian GNU/Hurd.</p>

<p>As imagens ISO de instalação podem ser baixadas do site
<a href="https://cdimage.debian.org/cdimage/ports/7.0/hurd-i386/">portes do Debian</a>
nos três "sabores" usuais do Debian: NETINST, CD ou DVD. Além do amigável
instalador do Debian, uma imagem de disco pré-instalada também está disponível,
proporcionando uma maneira ainda mais fácil de testar o Debian GNU/Hurd.

<p>O Debian GNU/Hurd atualmente está disponível para a arquitetura i386 e com
mais de 75% do repositório do Debian, e mais está por vir!</p>

<p>Por favor, certifique-se de ler as
<a href="https://www.debian.org/ports/hurd/hurd-install">informações de configuração</a>,
o <a href="https://www.gnu.org/software/hurd/faq.html">FAQ</a>
e a <a href="https://www.gnu.org/software/hurd/hurd/documentation/translator_primer.html">cartilha sobre tradutores</a>
para ter uma ideia das excelentes funcionalidades do GNU/Hurd.</p>

<p>Devido ao número muito pequeno de desenvolvedores(as),
nosso progresso no projeto não tem sido tão rápido quanto outros sistemas
operacionais bem sucedidos, mas nós acreditamos que alcançamos <a
href="https://www.gnu.org/software/hurd/hurd/status.html">um estado muito
decente</a>, mesmo com nossos recursos limitados. </p>

<p>Gostaríamos de agradecer a todas as pessoas que trabalharam no GNU/Hurd
<a href=https://www.gnu.org/software/hurd/history.html>nas últimas décadas</a>.
Nunca tivemos muitas pessoas trabalhando (e ainda não temos hoje, por favor,
<a href="https://www.gnu.org/software/hurd/contributing.html">participe</a>!),
mas no fim das contas muitas pessoas contribuíram de uma forma ou de outra.
<strong>Obrigado(a), pessoal!</strong></p>

<h3><:=spokendate ("2011-06-11"):></h3>

<p>Vários bugs da imagem baseada no debian-installer foram corrigidos, não
há problemas conhecidos, com exceção do GNOME e do KDE ainda não poderem
ser instalados.<br/>
Veja a <a href="./hurd-cd">página do CD do Hurd</a> para mais informações.</p>

<h3><:=spokendate ("2011-02-15"):></h3>

<p>A imagem baseada no debian-installer foi atualizada para os pacotes d-i do
squeeze.<br/>
Veja a <a href="./hurd-cd">página do CD do Hurd</a> para mais informações.</p>

<h3><:=spokendate ("2010-09-01"):></h3>

<p>Uma imagem baseada no debian-installer está disponível.<br/>
Veja a <a href="./hurd-cd">página do CD do Hurd</a> para mais informações.</p>

<h3><:=spokendate ("2009-10-19"):></h3>

<p>As imagens de DVD L1 agora estão disponíveis.<br/>
Veja a <a href="./hurd-cd">página do CD do Hurd</a> para mais informações.</p>

<h3><:=spokendate ("2007-12-21"):></h3>

<p>As imagens de CD K16 agora estão disponíveis.<br/>
Veja a <a href="./hurd-cd">página do CD do Hurd</a> para mais informações.</p>

<h3><:=spokendate ("2007-11-19"):></h3>

<p>As imagens de CD K15 agora estão disponíveis.<br/>
Veja a <a href="./hurd-cd">página do CD do Hurd</a> para mais informações.</p>

<h3><:=spokendate ("2006-11-27"):></h3>

<p>As imagens de CD K14 agora estão disponíveis.<br/>
Veja a <a href="./hurd-cd">página do CD do Hurd</a> para mais informações.</p>

<h3><:=spokendate ("2006-04-26"):></h3>

<p>As mini-imagens de CD K11 agora estão disponíveis.<br/>
Veja a <a href="./hurd-cd">página do CD do Hurd</a> para mais informações.</p>

<h3><:=spokendate ("2005-10-26"):></h3>

<p>As imagens de CD e DVD K10 agora estão disponíveis.<br/>
Veja a <a href="./hurd-cd">página do CD do Hurd</a> para mais informações.</p>

<h3><:=spokendate ("2005-05-14"):></h3>

<p>As imagens de CD K9 agora estão disponíveis.<br/>
Veja a <a href="./hurd-cd">página do CD do Hurd</a> para mais informações.</p>

<h3><:=spokendate ("2004-12-30"):></h3>

<P>As imagens de CD K8 agora estão disponíveis. Essas imagens suportam sistemas
de arquivos maiores que 2 GB e vêm com drivers de dispositivos de rede
atualizados.<br/>
Veja a <a href="./hurd-cd">página do CD do Hurd</a> para mais informações.</p>

<h3><:=spokendate ("2004-03-06"):></h3>

<P>Após um longo período sem serem atualizados, os novos snapshots CVS do <a
href="https://tracker.debian.org/pkg/hurd">Hurd</a> e do <a
href="https://tracker.debian.org/pkg/gnumach">GNU Mach</a> foram enviados.</p>

<h3><:=spokendate ("2003-07-31"):></h3>

<p>As imagens de CD K4 agora estão disponíveis.
Veja a <a href="./hurd-cd">página do CD do Hurd</a> para mais informações.</p>

<h3><:=spokendate ("2003-04-30"):></h3>

<p>As imagens de CD K3 agora estão disponíveis.
Elas foram renomeadas para GNU-K3-CDx.iso <br>
Veja a <a href="./hurd-cd">página do CD do Hurd</a> para mais informações.</p>


<h3><:=spokendate ("2003-03-06"):></h3>

<p>As imagens de CD K2 agora estão disponíveis.
Veja a <a href="./hurd-cd">página do CD do Hurd</a> para mais informações.</p>

<h3><:=spokendate ("2002-10-10"):></h3>

<p>As imagens de CD J2 agora estão disponíveis.
Veja a <a href="./hurd-cd">página do CD do Hurd</a> para mais informações.</p>

<P>Atualizando o Debian GNU/Hurd de um sistema baseado em libio antes de
12/08/2002 (incluindo a série de CD J1).

<P>Atualizar um sistema Debian GNU/Hurd em agosto de 2002 requer que se siga
o procedimento delineado no <A HREF="extra-files/hurd-upgrade.txt">manual de
atualização</A>.  Este procedimento de atualização é necessário porque as
interfaces do Hurd passaram por uma alteração incompatível ao se preparar para
o suporte a arquivos longos.

<h3><:=spokendate ("2002-08-05"):></h3>

<p>As imagens de CD J1 agora estão disponíveis.
Veja a <a href="./hurd-cd">página do CD do Hurd</a> para mais informações.</p>

<h3><:=spokendate ("2002-02-26"):></h3>

<p>As imagens de CD H3 agora estão disponíveis.
Veja a <a href="./hurd-cd">página do CD do Hurd</a> para mais informações.</p>

<h3><:=spokendate ("2001-12-15"):></h3>

<p>As imagens de CD H2 agora estão disponíveis.
Veja a <a href="./hurd-cd">página do CD do Hurd</a> para mais informações.</p>

<h3><:=spokendate ("2001-11-11"):></h3>

<p>As imagens de CD H1 agora estão disponíveis.
Veja a <a href="./hurd-cd">página do CD do Hurd</a> para mais informações.</p>

<h3><:=spokendate ("2001-10-05"):></h3>

<p>As imagens de CD G1 agora estão disponíveis.
Veja a <a href="./hurd-cd">página do CD do Hurd</a> para mais informações.</p>

<h3><:=spokendate ("2001-08-07"):></h3>

<p>
Hoje é a primeira vez que passamos a marca de 40% na <a
href="https://buildd.debian.org/stats/graph.png">estatística de
pacotes atualizados</a> por arquitetura.</p>

<h3><:=spokendate ("2001-07-31"):></h3>

<p>As imagens de CD F3 agora estão disponíveis.
Veja a <a href="./hurd-cd">página do CD do Hurd</a> para mais informações.</p>

<h3><:=spokendate ("2001-07-12"):></h3>

<p>O Marcus Brinkmann disponibilizou sua apresentação sobre o hurd.
Ela está disponível em:</p>
<a href="http://www.marcus-brinkmann.de/talks.en.html">http://www.marcus-brinkmann.de/talks.en.html</a>

<h3><:=spokendate ("1999-11-01"):></h3>

<p>
Limpamos alguns notórios relatórios de bugs que acabaram de ser fechados. Vamos
enviar alguns pacotes adicionais além daqueles enviados nos últimos dias
(inetutils, grub, man-db, o agora hostname, mtools, ...). Todos agora
compilam sem quaisquer alterações, o que é uma boa coisa. Parece que pouco a
pouco estamos estabilizando um conjunto básico de pacotes.</p>

<h3><:=spokendate ("1999-09-29"):></h3>

<p>
Agora existem <a
href="ftp://alpha.gnu.org/gnu/hurd/contrib/marcus/">patches para amarrar
dispositivos de caracteres Linux no GNU Mach</a>. Esses patches são muito
experimentais e o driver de tty não funciona corretamente com o tradutor
do term, mas estamos trabalhando nele e logo esperamos ser capazes de fornecer
um binário. Note que isso vai levar o console do Linux para o Hurd
(incluindo consoles em cor e virtuais), como também vai levar drivers para
vários mouses não padronizados e outros dispositivos seriais.</p>

<p>
Eu ouvi que algumas pessoas estão preocupadas com o tamanho do GNU Mach,
e também com ele se tornar um subconjunto do Linux. Por favor, note que
nós apenas estamos procurando por uma solução temporária aqui, até que
tenhamos tempo para redesenhar a interface do driver no GNU Mach (ou usar
um outro Microkernel). O microkernel não é tão importante assim como os
servidores do Hurd são, que rodam em cima dele.</p>

<p>
Pelo lado dos pacotes, agora temos um pacote <code>shadow</code> apropriado
(que fornece o <code>passwd</code> (obrigado(a), BenC!)). Além disso,
o <code>man-db</code> deve funcionar de imediato agora, mesmo com nomes de
arquivos longos, mas eu não verifiquei as outras alterações. De todo modo,
a seção básica está ficando legal.  Torin aplicou meu patch para o
<code>perl</code> e este é outro pacote que eu tenho que verificar se agora
pode ser 'finalizado'.</p>

<h3><:=spokendate ("1999-08-31"):></h3>

<p>
<code>debianutils 1.12</code> agora compila sem patch.</p>

<h3><:=spokendate ("1999-08-05"):></h3>

<p>
O código-fonte NMU para <code>passwd</code> (<code>shadow</code>) deve
corrigir todos os problemas restantes neste pacote. Isto deve levar a
uma instalação mais suave. Por outro lado, o <code>mutt</code> requer
um pequeno patch.</p>

<h3><:=spokendate ("1999-07-27"):></h3>

<p>
Os novos pacotes do sistema central foram finalizados. O Hurd tem uma nova
forma de realizar o boot (a parte que ocorre após o init ter iniciado); veja
em <code>/libexec/runsystem</code>. Isto é útil para o pacote
<code>sysvinit</code>, que está quase pronto. Corrigido um pequeno bug em
GNU Lib C e um no GNU Mach. A autodetecção de todas as placas de rede
deve funcionar agora, mas talvez tenhamos que mexer um pouco na ordem
(3c5x9 antes de 3c59x).</p>

<h3><:=spokendate ("1999-07-22"):></h3>

<p>
<code>cpio 2.4.2-25</code> corrige o problema remanescente de compatibilidade do
Hurd e agora pode ser compilado sem alterações. Obrigado(a), Brian!</p>

<h3><:=spokendate ("1999-07-05"):></h3>

<p>
Os patches do Perl 5.005.03 foram enviados ao(à) mantenedor(a). O código
original já estava limpo (obrigado(a), Mark!), mas os scripts de empacotamento
do Debian eram específicos ao Linux.</p>
