# translation of templates.ru.po to Russian
# Nick Toris <nicktoris@gmail.com>, 2007.
# Yuri Kozlov <kozlov.y@gmail.com>, 2008.
# Yuri Kozlov <yuray@komyakino.ru>, 2009, 2011, 2013.
# Lev Lamberov <l.lamberov@gmail.com>, 2014.
msgid ""
msgstr ""
"Project-Id-Version: Debian webwml templates\n"
"POT-Creation-Date: \n"
"PO-Revision-Date: 2020-12-28 22:30+0500\n"
"Last-Translator: Lev Lamberov <dogsleg@debian.org>\n"
"Language-Team: ru <debian-l10n-russian@lists.debian.org>\n"
"Language: ru\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: Poedit 2.4.2\n"
"Plural-Forms: nplurals=3; plural=(n%10==1 && n%100!=11 ? 0 : n%10>=2 && n"
"%10<=4 && (n%100<10 || n%100>=20) ? 1 : 2);\n"

#: ../../english/template/debian/basic.wml:19
#: ../../english/template/debian/navbar.wml:11
msgid "Debian"
msgstr "Debian"

#: ../../english/template/debian/basic.wml:48
msgid "Debian website search"
msgstr "Поиск по веб-сайту Debian"

#: ../../english/template/debian/common_translation.wml:4
msgid "Yes"
msgstr "Да"

#: ../../english/template/debian/common_translation.wml:7
msgid "No"
msgstr "Нет"

#: ../../english/template/debian/common_translation.wml:10
msgid "Debian Project"
msgstr "Проект Debian"

#: ../../english/template/debian/common_translation.wml:13
msgid ""
"Debian is an operating system and a distribution of Free Software. It is "
"maintained and updated through the work of many users who volunteer their "
"time and effort."
msgstr ""
"Debian &nbsp;&mdash; это операционная система и дистрибутив Свободного ПО. "
"Она сопровождается и обновляется трудом людей, добровольно вкладывающих в "
"это своё свободное время и свои усилия."

#: ../../english/template/debian/common_translation.wml:16
msgid "debian, GNU, linux, unix, open source, free, DFSG"
msgstr ""
"debian, дебиан, GNU, linux, линукс, unix, юникс, open source, free, "
"свободный, DFSG"

#: ../../english/template/debian/common_translation.wml:19
msgid "Back to the <a href=\"m4_HOME/\">Debian Project homepage</a>."
msgstr "Назад на домашнюю страницу проекта <a href=\"m4_HOME/\">Debian</a>."

#: ../../english/template/debian/common_translation.wml:22
#: ../../english/template/debian/links.tags.wml:149
msgid "Home"
msgstr "В&nbsp;начало"

#: ../../english/template/debian/common_translation.wml:25
msgid "Skip Quicknav"
msgstr "Пропустить QuickNav"

#: ../../english/template/debian/common_translation.wml:28
msgid "About"
msgstr "О&nbsp;Debian"

#: ../../english/template/debian/common_translation.wml:31
msgid "About Debian"
msgstr "О Debian"

#: ../../english/template/debian/common_translation.wml:34
msgid "Contact Us"
msgstr "Как с нами связаться"

#: ../../english/template/debian/common_translation.wml:37
msgid "Legal Info"
msgstr "Юридическая информация"

#: ../../english/template/debian/common_translation.wml:40
msgid "Data Privacy"
msgstr "Защита персональных данных"

#: ../../english/template/debian/common_translation.wml:43
msgid "Donations"
msgstr "Пожертвования"

#: ../../english/template/debian/common_translation.wml:46
msgid "Events"
msgstr "События"

#: ../../english/template/debian/common_translation.wml:49
msgid "News"
msgstr "Новости"

#: ../../english/template/debian/common_translation.wml:52
msgid "Distribution"
msgstr "Дистрибутив"

#: ../../english/template/debian/common_translation.wml:55
msgid "Support"
msgstr "Поддержка"

#: ../../english/template/debian/common_translation.wml:58
msgid "Pure Blends"
msgstr "Чистые смеси"

#: ../../english/template/debian/common_translation.wml:61
#: ../../english/template/debian/links.tags.wml:46
msgid "Developers' Corner"
msgstr "Уголок разработчика"

#: ../../english/template/debian/common_translation.wml:64
msgid "Documentation"
msgstr "Документация"

#: ../../english/template/debian/common_translation.wml:67
msgid "Security Information"
msgstr "Информация о безопасности"

#: ../../english/template/debian/common_translation.wml:70
msgid "Search"
msgstr "Поиск"

#: ../../english/template/debian/common_translation.wml:73
msgid "none"
msgstr "нет"

#: ../../english/template/debian/common_translation.wml:76
msgid "Go"
msgstr "Перейти"

#: ../../english/template/debian/common_translation.wml:79
msgid "worldwide"
msgstr "повсюду"

#: ../../english/template/debian/common_translation.wml:82
msgid "Site map"
msgstr "Карта сайта"

#: ../../english/template/debian/common_translation.wml:85
msgid "Miscellaneous"
msgstr "Разное"

#: ../../english/template/debian/common_translation.wml:88
#: ../../english/template/debian/links.tags.wml:104
msgid "Getting Debian"
msgstr "Где взять Debian"

#: ../../english/template/debian/common_translation.wml:91
msgid "The Debian Blog"
msgstr "Блог Debian"

#: ../../english/template/debian/common_translation.wml:94
msgid "Debian Micronews"
msgstr "Микроновости Debian"

#: ../../english/template/debian/common_translation.wml:97
msgid "Debian Planet"
msgstr "Планета Debian"

#: ../../english/template/debian/common_translation.wml:100
msgid "Last Updated"
msgstr "Последнее обновление"

#: ../../english/template/debian/ddp.wml:6
msgid ""
"Please send all comments, criticisms and suggestions about these web pages "
"to our <a href=\"mailto:debian-doc@lists.debian.org\">mailing list</a>."
msgstr ""
"Пожалуйста, направляйте все комментарии, критику и предложения, связанные с "
"этими страницами, в наш <a href=\"mailto:debian-doc@lists.debian.org"
"\">список рассылки</a>."

#: ../../english/template/debian/fixes_link.wml:11
msgid "not needed"
msgstr "не требуется"

#: ../../english/template/debian/fixes_link.wml:14
msgid "not available"
msgstr "недоступно"

#: ../../english/template/debian/fixes_link.wml:17
msgid "N/A"
msgstr "не применимо"

#: ../../english/template/debian/fixes_link.wml:20
msgid "in release 1.1"
msgstr "в выпуске 1.1"

#: ../../english/template/debian/fixes_link.wml:23
msgid "in release 1.3"
msgstr "в выпуске 1.3"

#: ../../english/template/debian/fixes_link.wml:26
msgid "in release 2.0"
msgstr "в выпуске 2.0"

#: ../../english/template/debian/fixes_link.wml:29
msgid "in release 2.1"
msgstr "в выпуске 2.1"

#: ../../english/template/debian/fixes_link.wml:32
msgid "in release 2.2"
msgstr "в выпуске 2.2"

#. TRANSLATORS: Please make clear in the translation of the following
#. item that mail sent to the debian-www list *must* be in English. Also,
#. you can add some information of your own translation mailing list
#. (i.e. debian-l10n-XXXXXX@lists.debian.org) for reporting things in
#. your language.
#: ../../english/template/debian/footer.wml:89
msgid ""
"To report a problem with the web site, please e-mail our publicly archived "
"mailing list <a href=\"mailto:debian-www@lists.debian.org\">debian-www@lists."
"debian.org</a> in English.  For other contact information, see the Debian <a "
"href=\"m4_HOME/contact\">contact page</a>. Web site source code is <a href="
"\"https://salsa.debian.org/webmaster-team/webwml\">available</a>."
msgstr ""
"Чтобы сообщить о проблеме, связанной с веб-сайтом, отправьте сообщение (на "
"английском) в публичный список рассылки по адресу <a href=\"mailto:debian-"
"www@lists.debian.org\">debian-www@lists.debian.org</a>. Об ошибках в русском "
"переводе сообщайте в русскоязычный список рассылки <a href=\"mailto:debian-"
"l10n-russian@lists.debian.org\">debian-l10n-russian@lists.debian.org</a>. "
"Прочую контактную информацию см. на странице <a href=\"m4_HOME/contact\">Как "
"с нами связаться</a>. Также доступен <a href=\"https://salsa.debian.org/"
"webmaster-team/webwml\">исходный код сайта</a>."

#: ../../english/template/debian/footer.wml:92
msgid "Last Modified"
msgstr "Последнее изменение"

#: ../../english/template/debian/footer.wml:95
msgid "Last Built"
msgstr "Последняя сборка"

#: ../../english/template/debian/footer.wml:98
msgid "Copyright"
msgstr "Авторские права"

#: ../../english/template/debian/footer.wml:101
msgid "<a href=\"https://www.spi-inc.org/\">SPI</a> and others;"
msgstr "<a href=\"https://www.spi-inc.org/\">SPI</a> и другие;"

#: ../../english/template/debian/footer.wml:104
msgid "See <a href=\"m4_HOME/license\" rel=\"copyright\">license terms</a>"
msgstr "См. <a href=\"m4_HOME/license\" rel=\"copyright\">условия лицензии</a>"

#: ../../english/template/debian/footer.wml:107
msgid ""
"Debian is a registered <a href=\"m4_HOME/trademark\">trademark</a> of "
"Software in the Public Interest, Inc."
msgstr ""
"Debian является зарегистрированным <a href=\"m4_HOME/trademark\">товарным "
"знаком</a> компании Software in the Public Interest, Inc. (Программное "
"обеспечение в интересах общества)"

#: ../../english/template/debian/languages.wml:196
#: ../../english/template/debian/languages.wml:232
msgid "This page is also available in the following languages:"
msgstr "Эта страница также доступна на следующих языках:"

#: ../../english/template/debian/languages.wml:265
msgid "How to set <a href=m4_HOME/intro/cn>the default document language</a>"
msgstr "Как установить <a href=m4_HOME/intro/cn>язык по умолчанию</a>"

#: ../../english/template/debian/languages.wml:323
msgid "Browser default"
msgstr "Браузер по умолчанию"

#: ../../english/template/debian/languages.wml:323
msgid "Unset the language override cookie"
msgstr "Сбросить куки изменения настроек языка"

#: ../../english/template/debian/links.tags.wml:4
msgid "Debian International"
msgstr "Международный Debian"

#: ../../english/template/debian/links.tags.wml:7
msgid "Partners"
msgstr "Партнёры"

#: ../../english/template/debian/links.tags.wml:10
msgid "Debian Weekly News"
msgstr "Еженедельные новости Debian"

#: ../../english/template/debian/links.tags.wml:13
msgid "Weekly News"
msgstr "Еженедельные новости"

#: ../../english/template/debian/links.tags.wml:16
msgid "Debian Project News"
msgstr "Новости проекта Debian"

#: ../../english/template/debian/links.tags.wml:19
msgid "Project News"
msgstr "Новости проекта"

#: ../../english/template/debian/links.tags.wml:22
msgid "Release Info"
msgstr "Информация о выпусках"

#: ../../english/template/debian/links.tags.wml:25
msgid "Debian Packages"
msgstr "Пакеты Debian"

#: ../../english/template/debian/links.tags.wml:28
msgid "Download"
msgstr "Загрузить"

#: ../../english/template/debian/links.tags.wml:31
msgid "Debian&nbsp;on&nbsp;CD"
msgstr "Debian&nbsp;на&nbsp;CD"

#: ../../english/template/debian/links.tags.wml:34
msgid "Debian Books"
msgstr "Книги о Debian"

#: ../../english/template/debian/links.tags.wml:37
msgid "Debian Wiki"
msgstr "Вики-страницы Debian"

#: ../../english/template/debian/links.tags.wml:40
msgid "Mailing List Archives"
msgstr "Архивы списков рассылки"

#: ../../english/template/debian/links.tags.wml:43
msgid "Mailing Lists"
msgstr "Списки рассылки"

#: ../../english/template/debian/links.tags.wml:49
msgid "Social Contract"
msgstr "Общественный договор"

#: ../../english/template/debian/links.tags.wml:52
msgid "Code of Conduct"
msgstr "Кодекс поведения"

#: ../../english/template/debian/links.tags.wml:55
msgid "Debian 5.0 - The universal operating system"
msgstr "Debian 5.0 - универсальная операционная система"

#: ../../english/template/debian/links.tags.wml:58
msgid "Site map for Debian web pages"
msgstr "Карта сайта Debian"

#: ../../english/template/debian/links.tags.wml:61
msgid "Developer Database"
msgstr "База данных о разработчиках"

#: ../../english/template/debian/links.tags.wml:64
msgid "Debian FAQ"
msgstr "ЧаВО Debian"

#: ../../english/template/debian/links.tags.wml:67
msgid "Debian Policy Manual"
msgstr "Руководство по политике Debian"

#: ../../english/template/debian/links.tags.wml:70
msgid "Developers' Reference"
msgstr "Справочник разработчика"

#: ../../english/template/debian/links.tags.wml:73
msgid "New Maintainers' Guide"
msgstr "Руководство нового разработчика"

#: ../../english/template/debian/links.tags.wml:76
msgid "Release Critical Bugs"
msgstr "Ошибки, критичные для выпуска"

#: ../../english/template/debian/links.tags.wml:79
msgid "Lintian Reports"
msgstr "Отчёты Lintian"

#: ../../english/template/debian/links.tags.wml:83
msgid "Archives for users' mailing lists"
msgstr "Архивы пользовательских списков рассылки"

#: ../../english/template/debian/links.tags.wml:86
msgid "Archives for developers' mailing lists"
msgstr "Архивы списков рассылки для разработчиков"

#: ../../english/template/debian/links.tags.wml:89
msgid "Archives for i18n/l10n mailing lists"
msgstr "Архивы списков рассылки по интернационализации и локализации"

#: ../../english/template/debian/links.tags.wml:92
msgid "Archives for ports' mailing lists"
msgstr "Архивы списков рассылки по переносам на разные архитектуры"

#: ../../english/template/debian/links.tags.wml:95
msgid "Archives for mailing lists of the Bug tracking system"
msgstr "Архивы списков рассылки по системе отслеживания ошибок"

#: ../../english/template/debian/links.tags.wml:98
msgid "Archives for miscellaneous mailing lists"
msgstr "Архивы других списков рассылки"

#: ../../english/template/debian/links.tags.wml:101
msgid "Free Software"
msgstr "Свободное ПО"

#: ../../english/template/debian/links.tags.wml:107
msgid "Development"
msgstr "Разработка"

#: ../../english/template/debian/links.tags.wml:110
msgid "Help Debian"
msgstr "Как помочь Debian"

#: ../../english/template/debian/links.tags.wml:113
msgid "Bug reports"
msgstr "Сообщения об ошибках"

#: ../../english/template/debian/links.tags.wml:116
msgid "Ports/Architectures"
msgstr "Перенос на другие архитектуры"

#: ../../english/template/debian/links.tags.wml:119
msgid "Installation manual"
msgstr "Инструкции по установке"

#: ../../english/template/debian/links.tags.wml:122
msgid "CD vendors"
msgstr "Поставщики CD"

#: ../../english/template/debian/links.tags.wml:125
msgid "CD/USB ISO images"
msgstr "ISO-образы CD/USB"

#: ../../english/template/debian/links.tags.wml:128
msgid "Network install"
msgstr "Установка по сети"

#: ../../english/template/debian/links.tags.wml:131
msgid "Pre-installed"
msgstr "Предустановка"

#: ../../english/template/debian/links.tags.wml:134
msgid "Debian-Edu project"
msgstr "Проект Debian в образовании (Debian-Edu)"

#: ../../english/template/debian/links.tags.wml:137
msgid "Alioth &ndash; Debian GForge"
msgstr "Alioth&nbsp;&mdash; GForge в Debian"

#: ../../english/template/debian/links.tags.wml:140
msgid "Quality Assurance"
msgstr "Контроль качества"

#: ../../english/template/debian/links.tags.wml:143
msgid "Package Tracking System"
msgstr "Система отслеживания пакетов"

#: ../../english/template/debian/links.tags.wml:146
msgid "Debian Developer's Packages Overview"
msgstr "Обзор пакетов с точки зрения разработчика Debian"

#: ../../english/template/debian/navbar.wml:10
msgid "Debian Home"
msgstr "Домашняя страница Debian"

#: ../../english/template/debian/recent_list.wml:7
msgid "No items for this year."
msgstr "Нет ничего за этот год."

#: ../../english/template/debian/recent_list.wml:11
msgid "proposed"
msgstr "предложенные"

#: ../../english/template/debian/recent_list.wml:15
msgid "in discussion"
msgstr "в процессе обсуждения"

#: ../../english/template/debian/recent_list.wml:19
msgid "voting open"
msgstr "голосование открыто"

#: ../../english/template/debian/recent_list.wml:23
msgid "finished"
msgstr "завершённые"

#: ../../english/template/debian/recent_list.wml:26
msgid "withdrawn"
msgstr "отменённые"

#: ../../english/template/debian/recent_list.wml:30
msgid "Future events"
msgstr "Будущие события"

#: ../../english/template/debian/recent_list.wml:33
msgid "Past events"
msgstr "Прошедшие события"

#: ../../english/template/debian/recent_list.wml:37
msgid "(new revision)"
msgstr "(новая редакция)"

#: ../../english/template/debian/recent_list.wml:329
msgid "Report"
msgstr "Дата сообщения"

#: ../../english/template/debian/redirect.wml:6
msgid "Page redirected to <newpage/>"
msgstr "Страница перенаправлена на <newpage/>"

#: ../../english/template/debian/redirect.wml:12
msgid ""
"This page has been renamed to <url <newpage/>>, please update your links."
msgstr ""
"Данная страница была переименована в <url <newpage/>>, обновите ваши ссылки."

#. given a manual name and an architecture, join them
#. if you need to reorder the two, use "%2$s ... %1$s", cf. printf(3)
#: ../../english/template/debian/release.wml:7
msgid "<void id=\"doc_for_arch\" />%s for %s"
msgstr "<void id=\"doc_for_arch\" />%s для %s"

#: ../../english/template/debian/translation-check.wml:37
msgid ""
"<em>Note:</em> The <a href=\"$link\">original document</a> is newer than "
"this translation."
msgstr ""
"<em>Замечание:</em> <a href=\"$link\">оригинал этого документа</a> новее, "
"чем перевод."

#: ../../english/template/debian/translation-check.wml:43
msgid ""
"Warning! This translation is too out of date, please see the <a href=\"$link"
"\">original</a>."
msgstr ""
"Предупреждение! Этот перевод сильно устарел, пользуйтесь, пожалуйста, <a "
"href=\"$link\">оригиналом</a>."

#: ../../english/template/debian/translation-check.wml:49
msgid ""
"<em>Note:</em> The original document of this translation no longer exists."
msgstr "<em>Замечание:</em> Оригинал этого документа больше не существует."

#: ../../english/template/debian/translation-check.wml:56
msgid "Wrong translation version!"
msgstr "Неправильная версия перевода!"

#: ../../english/template/debian/url.wml:4
msgid "URL"
msgstr "URL"

#: ../../english/template/debian/users.wml:12
msgid "Back to the <a href=\"../\">Who's using Debian? page</a>."
msgstr "Назад на страницу <a href=\"./\">Кто использует Debian</a>."

#: ../../english/search.xml.in:7
msgid "Debian website"
msgstr "Веб-сайт Debian"

#: ../../english/search.xml.in:9
msgid "Search the Debian website."
msgstr "Искать на веб-сайте Debian."

#~ msgid "Visit the site sponsor"
#~ msgstr "Посетите&nbsp;нашего&nbsp;спонсора"

#~ msgid "Rating:"
#~ msgstr "Рейтинг:"

#~ msgid "Nobody"
#~ msgstr "Никем"

#~ msgid "Taken by:"
#~ msgstr "Ответственный"

#~ msgid "More information:"
#~ msgstr "Подробнее:"

#~ msgid "Select a server near you: &nbsp;"
#~ msgstr "Выберите ближайший сервер: &nbsp;"

#~ msgid "Report it!"
#~ msgstr "Сообщите об этом!"

#~ msgid "Have you found a problem with the site layout?"
#~ msgstr "Проблема с отображением сайта?"
