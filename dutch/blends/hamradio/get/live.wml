#use wml::debian::blend title="Live-images downloaden"
#use wml::debian::blends::hamradio
#use "../navbar.inc"
#use wml::debian::translation-check translation="a2a93c6af14d9de0d3c3aa2b2d7fa4d06a48ee43"

<p>Voor de doelgroepspecifieke collectie Hamradio van Debian worden
<b>live dvd-images</b> geproduceerd, welke gebruikt kunnen worden om deze
doelgroepspecifieke collectie Hamradio van Debian uit te proberen op een
computer zonder dat deze eerst geïnstalleerd moet worden. De images bevatten ook
een installatieprogramma dat gebruikt kan worden om Debian te installeren samen
met de pakketten uit de doelgroepspecifieke uitgave.</p>

<h2>Een voorbeelduitgave van de Debian Hamradio-collectie voor Jessie</h2>

<p>Men kan een live-dvd met een voorbeelduitgave van de Debian
Hamradio-collectie voor Jessie downloaden. Dit is een  niet-officiële uitgave,
omdat de metapakketten voor deze doelgroepspecifieke collectie niet opgenomen
zijn in Debian Jessie.</p>

<p>De laatste stabiele release is: <strong><stable-version/></strong>.</p>

<ul>
        <li><a href="<stable-amd64-url/>">Live dvd-image voor amd64(ISO)</a>
        <li><a href="<stable-i386-url/>">Live dvd-image voor i386 (ISO)</a>
        <li><a href="<stable-source-url/>">Archiefbestand met de broncode van het live dvd-image (tar)</a>
</ul>

<p>Raadpleeg de <a href="https://cdimage.debian.org/cdimage/blends-live/">
volledige bestandenlijst</a> voor webboot-images, checksums en GPG-handtekeningen.</p>

<h2>Debian Hamradio-collectie voor Stretch</h2>

<p>In de nabije toekomst zullen live dvd's voor stretch (de huidige
testing-distributie in Debian) gebouwd worden, hoewel deze momenteel nog niet beschikbaar zijn.</p>

<h2>Aan de slag</h2>

<h3>met een dvd</h3>

<p>De meeste moderne besturingssystemen hebben voorzieningen voor het branden
van ISO-images op dvd-media. De cd-FAQ van Debian bevat instructies voor het
branden van ISO-images onder <a href="https://www.debian.org/CD/faq/index#record-unix">Linux</a>, <a
href="https://www.debian.org/CD/faq/index#record-windows">Windows</a> en <a
href="https://www.debian.org/CD/faq/index#record-mac">Mac OS</a>. Indien u moeilijkheden ondervindt, zou u met een zoekmachine op het web de nodige antwoorden moeten kunnen vinden.</p>

<h3>met een USB-stick</h3>

<p>De ISO-images worden als hybride images gebouwd, waardoor u deze rechtstreeks op een USB-stick kunt kopiëren zonder speciale software zoals unetbootin te moeten gebruiken. Op een Linux-systeem, kunt u dit als volgt doen:</p>

<pre>sudo dd if=/pad/naar/debian-hamradio-live-image.iso of=/dev/sd<b>X</b></pre>

<p>De uitvoer van het commando dmesg zou u moeten toelaten te weten welke de
apparaatnaam is van de USB-stick, waarbij u de <b>X</b> zult moeten vervangen
door de verkregen letter.</p>

