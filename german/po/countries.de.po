# German translation of the Debian webwml modules
# Copyright (c) 2004 Software in the Public Interest, Inc.
# Dr. Tobias Quathamer <toddy@debian.org>, 2004, 2005, 2006, 2007, 2012, 2017.
# Gerfried Fuchs <rhonda@debian.at>, 2002, 2003, 2004, 2006, 2008, 2010.
# Helge Kreutzmann <debian@helgefjell.de>, 2007, 2008.
# Holger Wansing <linux@wansing-online.de>, 2011.
msgid ""
msgstr ""
"Project-Id-Version: Debian webwml countries\n"
"PO-Revision-Date: 2017-05-15 22:56+0100\n"
"Last-Translator: Dr. Tobias Quathamer <toddy@debian.org>\n"
"Language-Team: German <debian-l10n-german@lists.debian.org>\n"
"Language: de\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms:  nplurals=2; plural=(n != 1);\n"
"X-Generator: Lokalize 2.0\n"

#: ../../english/template/debian/countries.wml:111
msgid "United Arab Emirates"
msgstr "Vereinigte Arabische Emirate"

#: ../../english/template/debian/countries.wml:114
msgid "Albania"
msgstr "Albanien"

#: ../../english/template/debian/countries.wml:117
msgid "Armenia"
msgstr "Armenien"

#: ../../english/template/debian/countries.wml:120
msgid "Argentina"
msgstr "Argentinien"

#: ../../english/template/debian/countries.wml:123
msgid "Austria"
msgstr "Österreich"

#: ../../english/template/debian/countries.wml:126
msgid "Australia"
msgstr "Australien"

#: ../../english/template/debian/countries.wml:129
msgid "Bosnia and Herzegovina"
msgstr "Bosnien und Herzegowina"

#: ../../english/template/debian/countries.wml:132
msgid "Bangladesh"
msgstr "Bangladesch"

#: ../../english/template/debian/countries.wml:135
msgid "Belgium"
msgstr "Belgien"

#: ../../english/template/debian/countries.wml:138
msgid "Bulgaria"
msgstr "Bulgarien"

#: ../../english/template/debian/countries.wml:141
msgid "Brazil"
msgstr "Brasilien"

#: ../../english/template/debian/countries.wml:144
msgid "Bahamas"
msgstr "Bahamas"

#: ../../english/template/debian/countries.wml:147
msgid "Belarus"
msgstr "Weißrussland"

#: ../../english/template/debian/countries.wml:150
msgid "Canada"
msgstr "Kanada"

#: ../../english/template/debian/countries.wml:153
msgid "Switzerland"
msgstr "Schweiz"

#: ../../english/template/debian/countries.wml:156
msgid "Chile"
msgstr "Chile"

#: ../../english/template/debian/countries.wml:159
msgid "China"
msgstr "China"

#: ../../english/template/debian/countries.wml:162
msgid "Colombia"
msgstr "Kolumbien"

#: ../../english/template/debian/countries.wml:165
msgid "Costa Rica"
msgstr "Costa Rica"

#: ../../english/template/debian/countries.wml:168
msgid "Czech Republic"
msgstr "Tschechische Republik"

#: ../../english/template/debian/countries.wml:171
msgid "Germany"
msgstr "Deutschland"

#: ../../english/template/debian/countries.wml:174
msgid "Denmark"
msgstr "Dänemark"

#: ../../english/template/debian/countries.wml:177
msgid "Dominican Republic"
msgstr "Dominikanische Republik"

#: ../../english/template/debian/countries.wml:180
msgid "Algeria"
msgstr "Algerien"

#: ../../english/template/debian/countries.wml:183
msgid "Ecuador"
msgstr "Ecuador"

#: ../../english/template/debian/countries.wml:186
msgid "Estonia"
msgstr "Estland"

#: ../../english/template/debian/countries.wml:189
msgid "Egypt"
msgstr "Ägypten"

#: ../../english/template/debian/countries.wml:192
msgid "Spain"
msgstr "Spanien"

#: ../../english/template/debian/countries.wml:195
msgid "Ethiopia"
msgstr "Äthiopien"

#: ../../english/template/debian/countries.wml:198
msgid "Finland"
msgstr "Finnland"

#: ../../english/template/debian/countries.wml:201
msgid "Faroe Islands"
msgstr "Färöer-Inseln"

#: ../../english/template/debian/countries.wml:204
msgid "France"
msgstr "Frankreich"

#: ../../english/template/debian/countries.wml:207
msgid "United Kingdom"
msgstr "Vereinigtes Königreich"

#: ../../english/template/debian/countries.wml:210
msgid "Grenada"
msgstr "Grenada"

#: ../../english/template/debian/countries.wml:213
msgid "Georgia"
msgstr "Georgien"

#: ../../english/template/debian/countries.wml:216
msgid "Greenland"
msgstr "Grönland"

#: ../../english/template/debian/countries.wml:219
msgid "Greece"
msgstr "Griechenland"

#: ../../english/template/debian/countries.wml:222
msgid "Guatemala"
msgstr "Guatemala"

#: ../../english/template/debian/countries.wml:225
msgid "Hong Kong"
msgstr "Hong Kong"

#: ../../english/template/debian/countries.wml:228
msgid "Honduras"
msgstr "Honduras"

#: ../../english/template/debian/countries.wml:231
msgid "Croatia"
msgstr "Kroatien"

#: ../../english/template/debian/countries.wml:234
msgid "Hungary"
msgstr "Ungarn"

#: ../../english/template/debian/countries.wml:237
msgid "Indonesia"
msgstr "Indonesien"

#: ../../english/template/debian/countries.wml:240
msgid "Iran"
msgstr "Iran"

#: ../../english/template/debian/countries.wml:243
msgid "Ireland"
msgstr "Irland"

#: ../../english/template/debian/countries.wml:246
msgid "Israel"
msgstr "Israel"

#: ../../english/template/debian/countries.wml:249
msgid "India"
msgstr "Indien"

#: ../../english/template/debian/countries.wml:252
msgid "Iceland"
msgstr "Island"

#: ../../english/template/debian/countries.wml:255
msgid "Italy"
msgstr "Italien"

#: ../../english/template/debian/countries.wml:258
msgid "Jordan"
msgstr "Jordanien"

#: ../../english/template/debian/countries.wml:261
msgid "Japan"
msgstr "Japan"

#: ../../english/template/debian/countries.wml:264
msgid "Kenya"
msgstr "Kenia"

#: ../../english/template/debian/countries.wml:267
#, fuzzy
msgid "Kyrgyzstan"
msgstr "Kasachstan"

#: ../../english/template/debian/countries.wml:270
msgid "Cambodia"
msgstr ""

#: ../../english/template/debian/countries.wml:273
msgid "Korea"
msgstr "Korea"

#: ../../english/template/debian/countries.wml:276
msgid "Kuwait"
msgstr "Kuwait"

#: ../../english/template/debian/countries.wml:279
msgid "Kazakhstan"
msgstr "Kasachstan"

#: ../../english/template/debian/countries.wml:282
msgid "Sri Lanka"
msgstr "Sri Lanka"

#: ../../english/template/debian/countries.wml:285
msgid "Lithuania"
msgstr "Litauen"

#: ../../english/template/debian/countries.wml:288
msgid "Luxembourg"
msgstr "Luxemburg"

#: ../../english/template/debian/countries.wml:291
msgid "Latvia"
msgstr "Lettland"

#: ../../english/template/debian/countries.wml:294
msgid "Morocco"
msgstr "Marokko"

#: ../../english/template/debian/countries.wml:297
msgid "Moldova"
msgstr "Moldawien"

#: ../../english/template/debian/countries.wml:300
msgid "Montenegro"
msgstr "Montenegro"

#: ../../english/template/debian/countries.wml:303
msgid "Madagascar"
msgstr "Madagaskar"

#: ../../english/template/debian/countries.wml:306
msgid "Macedonia, Republic of"
msgstr "Republik Mazedonien"

#: ../../english/template/debian/countries.wml:309
msgid "Mongolia"
msgstr "Mongolei"

#: ../../english/template/debian/countries.wml:312
msgid "Malta"
msgstr "Malta"

#: ../../english/template/debian/countries.wml:315
msgid "Mexico"
msgstr "Mexiko"

#: ../../english/template/debian/countries.wml:318
msgid "Malaysia"
msgstr "Malaysia"

#: ../../english/template/debian/countries.wml:321
msgid "New Caledonia"
msgstr "Neukaledonien"

#: ../../english/template/debian/countries.wml:324
msgid "Nicaragua"
msgstr "Nicaragua"

#: ../../english/template/debian/countries.wml:327
msgid "Netherlands"
msgstr "Niederlande"

#: ../../english/template/debian/countries.wml:330
msgid "Norway"
msgstr "Norwegen"

#: ../../english/template/debian/countries.wml:333
msgid "New Zealand"
msgstr "Neuseeland"

#: ../../english/template/debian/countries.wml:336
msgid "Panama"
msgstr "Panama"

#: ../../english/template/debian/countries.wml:339
msgid "Peru"
msgstr "Peru"

#: ../../english/template/debian/countries.wml:342
msgid "French Polynesia"
msgstr "Französisch-Polynesien"

#: ../../english/template/debian/countries.wml:345
msgid "Philippines"
msgstr "Philippinen"

#: ../../english/template/debian/countries.wml:348
msgid "Pakistan"
msgstr "Pakistan"

#: ../../english/template/debian/countries.wml:351
msgid "Poland"
msgstr "Polen"

#: ../../english/template/debian/countries.wml:354
msgid "Portugal"
msgstr "Portugal"

#: ../../english/template/debian/countries.wml:357
msgid "Réunion"
msgstr "Réunion"

#: ../../english/template/debian/countries.wml:360
msgid "Romania"
msgstr "Rumänien"

#: ../../english/template/debian/countries.wml:363
msgid "Serbia"
msgstr "Serbien"

#: ../../english/template/debian/countries.wml:366
msgid "Russia"
msgstr "Russland"

#: ../../english/template/debian/countries.wml:369
msgid "Saudi Arabia"
msgstr "Saudi-Arabien"

#: ../../english/template/debian/countries.wml:372
msgid "Sweden"
msgstr "Schweden"

#: ../../english/template/debian/countries.wml:375
msgid "Singapore"
msgstr "Singapur"

#: ../../english/template/debian/countries.wml:378
msgid "Slovenia"
msgstr "Slowenien"

#: ../../english/template/debian/countries.wml:381
msgid "Slovakia"
msgstr "Slowakei"

#: ../../english/template/debian/countries.wml:384
msgid "El Salvador"
msgstr "El Salvador"

#: ../../english/template/debian/countries.wml:387
msgid "Thailand"
msgstr "Thailand"

#: ../../english/template/debian/countries.wml:390
msgid "Tajikistan"
msgstr "Tadschikistan"

#: ../../english/template/debian/countries.wml:393
msgid "Tunisia"
msgstr "Tunesien"

#: ../../english/template/debian/countries.wml:396
msgid "Turkey"
msgstr "Türkei"

#: ../../english/template/debian/countries.wml:399
msgid "Taiwan"
msgstr "Taiwan"

#: ../../english/template/debian/countries.wml:402
msgid "Ukraine"
msgstr "Ukraine"

#: ../../english/template/debian/countries.wml:405
msgid "United States"
msgstr "Vereinigte Staaten von Amerika"

#: ../../english/template/debian/countries.wml:408
msgid "Uruguay"
msgstr "Uruguay"

#: ../../english/template/debian/countries.wml:411
msgid "Uzbekistan"
msgstr "Usbekistan"

#: ../../english/template/debian/countries.wml:414
msgid "Venezuela"
msgstr "Venezuela"

#: ../../english/template/debian/countries.wml:417
msgid "Vietnam"
msgstr "Vietnam"

#: ../../english/template/debian/countries.wml:420
msgid "Vanuatu"
msgstr "Vanuatu"

#: ../../english/template/debian/countries.wml:423
msgid "South Africa"
msgstr "Südafrika"

#: ../../english/template/debian/countries.wml:426
msgid "Zimbabwe"
msgstr "Simbabwe"

#~ msgid "Great Britain"
#~ msgstr "Großbritannien"
