#use wml::debian::template title="Unterstützung"
#use wml::debian::toc
#use wml::debian::translation-check translation="6838e6aa35cea0dd360ea9a9f08965ebdc8c9d50"
# Translator: Philipp Frauenfelder <pfrauenf@debian.org>
# Updated by: Thimo Neubauer <thimo@debian.org>
# Updated by: Holger Wansing <linux@wansing-online.de>, 2011, 2016, 2018.
# Updated by: Holger Wansing <hwansing@mailbox.org>, 2019, 2020.

<define-tag toc-title-formatting endtag="required" whitespace="delete">
<h2>%body</h2>
</define-tag>

<p>
Debian und seine Benutzerunterstützung wird von einer Gemeinschaft Freiwilliger
betrieben.
Falls diese Unterstützung nicht Ihren Anforderungen entspricht, sollten Sie
vielleicht unsere <a href="doc/">Dokumentation</a> lesen oder einen der
<a href="consultants/">Berater</a> konsultieren.
</p>

<toc-display />

<toc-add-entry name="irc">Online Echtzeit-Hilfe über IRC</toc-add-entry>

<p>
<a href="http://www.irchelp.org/">IRC (Internet Relay Chat)</a> ist
eine Möglichkeit, in Echtzeit mit anderen Personen auf der
gesamten Welt zu kommunizieren. Debian betreffende IRC-Kanäle finden Sie auf der <a
href="https://www.oftc.net/">OFTC</a>-Website.
</p>

<p>
Um sich mit dem Netzwerk zu verbinden, benötigen Sie einen
IRC-Client. Einige der beliebtesten sind
<a href="https://packages.debian.org/stable/net/hexchat">HexChat</a>,
<a href="https://packages.debian.org/stable/net/ircii">ircII</a>,
<a href="https://packages.debian.org/stable/net/irssi">irssi</a>,
<a href="https://packages.debian.org/stable/net/epic5">epic5</a> und
<a href="https://packages.debian.org/stable/net/kvirc">KVIrc</a>,
die alle für Debian paketiert wurden. OFTC bietet auch ein
<a href="https://www.oftc.net/WebChat/">WebChat</a>-Interface an,
über das Sie sich in einem Browser mit dem IRC verbinden können,
ohne lokal ein Client-Programm installieren zu müssen.
</p>

<p>
Wenn Sie den Client installiert haben, müssen Sie ihm mitteilen, sich mit
dem Server zu verbinden.
In den meisten Clients erreichen Sie dies, in dem Sie Folgendes
eingeben:
</p>

<pre>
/server irc.debian.org
</pre>

<p>
In einigen Clients (wie z.B. irssi) müssen Sie stattdessen dies verwenden:
</p>

<pre>
/connect irc.debian.org
</pre>

# Note to translators:
# You might want to insert here a paragraph stating which IRC channel is available
# for user support in your language and pointing to the English IRC channel.
# <p>Once you are connected, join channel <code>#debian-foo</code> by typing</p>
# <pre>/join #debian</pre>
# for support in your language.
# <p>For support in English, read on</p>

<p>
Sobald Sie verbunden sind, können Sie dem Kanal
<code>#debian.de</code> durch Eingabe von
</p>

<pre>
/join #debian.de
</pre>

<p>
beitreten, um Unterstützung in deutscher Sprache zu erhalten.
</p>

<p>
Hilfe in englischer Sprache erhalten Sie auf dem Kanal
<code>#debian</code> durch Eingabe von
</p>

<pre>
/join #debian
</pre>

<p>
Sie finden sich jetzt inmitten der freundlichen
Gemeinschaft der Bewohner von <code>#debian.de</code> (bzw.
<code>#debian</code>) wieder, und dürfen gerne Fragen über Debian stellen.
Die <code>#debian</code>-FAQ
finden Sie unter <url "https://wiki.debian.org/DebianIRC" />.
</p>

<p>
Beachten Sie: viele Clients wie HexChat haben eine andere (grafische)
Oberfläche, um sich mit Servern und Kanälen zu verbinden.
</p>

<toc-add-entry name="mail_lists" href="MailingLists/">Mailinglisten</toc-add-entry>

<p>
Debian wird über die ganze Welt verteilt
entwickelt. Daher ist E-Mail das bevorzugte Medium, um verschiedenste
Dinge zu diskutieren. Der Großteil der Kommunikation zwischen Debian-Entwicklern
und -Benutzern wird über verschiedene Mailinglisten abgewickelt.
</p>

<p>
Es gibt verschiedene öffentliche Listen. Mehr Informationen finden Sie auf
Debians <a href="MailingLists/">Mailinglisten</a>-Seite.
</p>

# Note to translators:
# You might want to adapt the following paragraph, stating which list
# is available for user support in your language instead of English.

<p>
Bezüglich der Unterstützung für Benutzer in deutscher Sprache kontaktieren Sie
bitte die Mailingliste
<a href="https://lists.debian.org/debian-user-german/">debian-user-german</a>.
</p>

<p>
Falls Sie Unterstützung in einer anderen Sprache benötigen, suchen Sie bitte
im <a href="https://lists.debian.org/users.html">Mailinglisten-Index</a> nach
<q>user</q>.
</p>

<p>
Es gibt natürlich noch viele andere Mailinglisten, die sich jeweils unterschiedlichen
Aspekten des weitläufigen Linux-Ökosystems widmen und nicht
Debian-spezifisch sind. Verwenden Sie Ihre bevorzugte Suchmaschine,
um die für Ihre Zwecke am besten geeignete Liste zu finden.
</p>


<toc-add-entry name="usenet">Usenet Newsgroups</toc-add-entry>

<p>
Viele unserer <a href="#mail_lists">Mailinglisten</a> können als
Newsgroups gelesen werden, suchen Sie dazu in der <kbd>linux.debian.*</kbd>
Hierarchie. Oder nutzen Sie ein Web-Interface wie
<a href="https://groups.google.com/forum/">Google Groups</a>.
</p>


<toc-add-entry name="web">Websites</toc-add-entry>

<h3 id="forums">Foren</h3>

# Note to translators:
# If there is a specific Debian forum for your language you might want to
# insert here a paragraph stating which list is available for user support
# in your language and pointing to the English forums.
# <p><a href="http://someforum.example.org/">someforum</a> is a web portal
# on which you can use your language to discuss Debian-related topics,
# submit questions about Debian, and have them answered by other users.</p>
#
# <p><a href="http://forums.debian.net">Debian User Forums</a> is a web portal
# on which you can use the English language to discuss Debian-related topics,
# submit questions about Debian, and have them answered by other users.</p>

<p>
<a href="https://debianforum.de/forum/">Debianforum.de</a> ist ein
Webforum, in dem Sie in deutscher Sprache Themen mit Bezug zu Debian
diskutieren sowie Fragen über Debian stellen können und diese von anderen
Benutzern beantwortet bekommen.
</p>

<p>
<a href="http://forums.debian.net">Debian User Forums</a> ist ein Webportal
mit ähnlichen Zielsetzungen, jedoch wird dort die englische Sprache genutzt.
</p>


<toc-add-entry name="maintainers">Kontakt zu Paketbetreuern</toc-add-entry>

<p>
Es gibt zwei Wege, den Betreuer eines Pakets zu erreichen. Wenn Sie
dem Betreuer einen Fehler in seinem Paket mitteilen wollen,
füllen Sie einfach einen Fehlerbericht aus (mehr darüber im folgenden
Abschnitt über die Fehlerdatenbank). Der
Betreuer erhält eine Kopie des Fehlerberichts.
</p>

<p>
Wenn Sie einfach mit dem Betreuer kommunizieren wollen, können
Sie die speziellen Mail-Alias-Adressen für die jeweiligen Pakete
benutzen. Jede E-Mail, die Sie an
&lt;paketname&gt;@packages.debian.org schicken, wird an den
zuständigen Betreuer weitergeleitet.
</p>


<toc-add-entry name="bts" href="Bugs/">Die Fehlerdatenbank</toc-add-entry>

<p>
Die Debian-Distribution betreibt eine Fehlerdatenbank, welche
alle Details von Fehlern, die von Entwicklern oder Benutzern gemeldet
wurden, festhält. Jeder Fehler erhält eine Nummer und bleibt solange
im System aktiv, bis der Fehler als behoben erklärt wird.
</p>

<p>
Um einen Fehler zu melden, besuchen Sie die unten aufgeführte
Seite der Fehlerdatenbank; Sie können das Paket <q>reportbug</q>
zur automatischen Erzeugung eines Fehlerbericht benutzen.
</p>

<p>
Mehr Informationen über Fehlerberichte, wie man nach
derzeit offenen Fehlern sucht sowie Infos über die Fehlerdatenbank im
Allgemeinen erhalten Sie auf der <a href="Bugs/">Webseite der
Debian-Fehlerdatenbank</a>.
</p>


<toc-add-entry name="consultants" href="consultants/">Berater</toc-add-entry>

<p>
Debian ist Freie Software und offeriert kostenlose Hilfe über die
Mailinglisten. Es gibt aber Leute, die keine Zeit dazu haben oder spezielle
Bedürfnisse, und gewillt sind, jemanden zu engagieren
für die Wartung oder Erweiterung ihres Debian-Systems. Auf der
<a href="consultants/">Berater-Seite</a> haben wir eine Liste von
Leuten bzw. Firmen zusammengestellt, die einen solchen Service anbieten.
</p>


<toc-add-entry name="release" href="releases/stable/">Bekannte Probleme</toc-add-entry>

<p>
Einschränkungen und gravierende Probleme der aktuellen Stable-Distribution
(sofern vorhanden) werden auf den <a href="releases/stable/">Seiten zur
Stable-Veröffentlichung</a> aufgeführt.
</p>

<p>
Sie sollten vor allem die
<a href="releases/stable/releasenotes">Veröffentlichungshinweise</a>
und die <a href="releases/stable/errata">Errata</a> (Auflistung bekannter Fehler) lesen.
</p>
