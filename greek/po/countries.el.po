# Emmanuel Galatoulas <galas@tee.gr>, 2019, 2020.
# EG <galatoulas@cti.gr>, 2021.
msgid ""
msgstr ""
"Project-Id-Version: Debian webwml organization\n"
"PO-Revision-Date: 2021-01-22 03:08+0200\n"
"Last-Translator: EG <galatoulas@cti.gr>\n"
"Language-Team: Greek <debian-l10n-greek@lists.debian.org>\n"
"Language: el\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"
"X-Generator: Lokalize 20.12.0\n"

#: ../../english/template/debian/countries.wml:111
msgid "United Arab Emirates"
msgstr "Ηνωμένα Αραβικά Εμιράτα"

#: ../../english/template/debian/countries.wml:114
msgid "Albania"
msgstr "Αλβανία"

#: ../../english/template/debian/countries.wml:117
msgid "Armenia"
msgstr "Αρμενία"

#: ../../english/template/debian/countries.wml:120
msgid "Argentina"
msgstr "Αργεντινή"

#: ../../english/template/debian/countries.wml:123
msgid "Austria"
msgstr "Αυστρία"

#: ../../english/template/debian/countries.wml:126
msgid "Australia"
msgstr "Αυστραλία"

#: ../../english/template/debian/countries.wml:129
msgid "Bosnia and Herzegovina"
msgstr "Βοσνία και Ερζεγοβίνη"

#: ../../english/template/debian/countries.wml:132
msgid "Bangladesh"
msgstr "Μπαγκλαντές"

#: ../../english/template/debian/countries.wml:135
msgid "Belgium"
msgstr "Βέλγιο"

#: ../../english/template/debian/countries.wml:138
msgid "Bulgaria"
msgstr "Βουλγαρία"

#: ../../english/template/debian/countries.wml:141
msgid "Brazil"
msgstr "Βραζιλία"

#: ../../english/template/debian/countries.wml:144
msgid "Bahamas"
msgstr "Μπαχάμες"

#: ../../english/template/debian/countries.wml:147
msgid "Belarus"
msgstr "Λευκορωσία"

#: ../../english/template/debian/countries.wml:150
msgid "Canada"
msgstr "Καναδάς"

#: ../../english/template/debian/countries.wml:153
msgid "Switzerland"
msgstr "Ελβετία"

#: ../../english/template/debian/countries.wml:156
msgid "Chile"
msgstr "Χιλή"

#: ../../english/template/debian/countries.wml:159
msgid "China"
msgstr "Κίνα"

#: ../../english/template/debian/countries.wml:162
msgid "Colombia"
msgstr "Κολομβία"

#: ../../english/template/debian/countries.wml:165
msgid "Costa Rica"
msgstr "Κόστα Ρίκα"

#: ../../english/template/debian/countries.wml:168
msgid "Czech Republic"
msgstr "Τσεχική Δημοκρατία"

#: ../../english/template/debian/countries.wml:171
msgid "Germany"
msgstr "Γερμανία"

#: ../../english/template/debian/countries.wml:174
msgid "Denmark"
msgstr "Δανία"

#: ../../english/template/debian/countries.wml:177
msgid "Dominican Republic"
msgstr "Δομινικανή Δημοκρατία"

#: ../../english/template/debian/countries.wml:180
msgid "Algeria"
msgstr "Αλγερία"

#: ../../english/template/debian/countries.wml:183
msgid "Ecuador"
msgstr "Ισημερινός"

#: ../../english/template/debian/countries.wml:186
msgid "Estonia"
msgstr "Εσθονία"

#: ../../english/template/debian/countries.wml:189
msgid "Egypt"
msgstr "Αίγυπτος"

#: ../../english/template/debian/countries.wml:192
msgid "Spain"
msgstr "Ισπανία"

#: ../../english/template/debian/countries.wml:195
msgid "Ethiopia"
msgstr "Εθιοπία"

#: ../../english/template/debian/countries.wml:198
msgid "Finland"
msgstr "Φινλανδία"

#: ../../english/template/debian/countries.wml:201
msgid "Faroe Islands"
msgstr "Νήσοι Φερόες"

#: ../../english/template/debian/countries.wml:204
msgid "France"
msgstr "Γαλλία"

#: ../../english/template/debian/countries.wml:207
msgid "United Kingdom"
msgstr "Ην. Βασίλειο"

#: ../../english/template/debian/countries.wml:210
msgid "Grenada"
msgstr "Γρενάδα"

#: ../../english/template/debian/countries.wml:213
msgid "Georgia"
msgstr "Γεωργία"

#: ../../english/template/debian/countries.wml:216
msgid "Greenland"
msgstr "Γροιλανδία"

#: ../../english/template/debian/countries.wml:219
msgid "Greece"
msgstr "Ελλάδα"

#: ../../english/template/debian/countries.wml:222
msgid "Guatemala"
msgstr "Γουατεμάλα"

#: ../../english/template/debian/countries.wml:225
msgid "Hong Kong"
msgstr "Χονκ Κονκ"

#: ../../english/template/debian/countries.wml:228
msgid "Honduras"
msgstr "Ονδούρες"

#: ../../english/template/debian/countries.wml:231
msgid "Croatia"
msgstr "Κροατία"

#: ../../english/template/debian/countries.wml:234
msgid "Hungary"
msgstr "Ουγγαρία"

#: ../../english/template/debian/countries.wml:237
msgid "Indonesia"
msgstr "Ινδονησία"

#: ../../english/template/debian/countries.wml:240
msgid "Iran"
msgstr "Ιράν"

#: ../../english/template/debian/countries.wml:243
msgid "Ireland"
msgstr "Ιρλανδία"

#: ../../english/template/debian/countries.wml:246
msgid "Israel"
msgstr "Ισραήλ"

#: ../../english/template/debian/countries.wml:249
msgid "India"
msgstr "Ινδία"

#: ../../english/template/debian/countries.wml:252
msgid "Iceland"
msgstr "Ισλανδία"

#: ../../english/template/debian/countries.wml:255
msgid "Italy"
msgstr "Ιταλία"

#: ../../english/template/debian/countries.wml:258
msgid "Jordan"
msgstr "Ιορδανία"

#: ../../english/template/debian/countries.wml:261
msgid "Japan"
msgstr "Ιαπωνία"

#: ../../english/template/debian/countries.wml:264
msgid "Kenya"
msgstr "Κένυα"

#: ../../english/template/debian/countries.wml:267
msgid "Kyrgyzstan"
msgstr "Κιρζιγιστάν"

#: ../../english/template/debian/countries.wml:270
msgid "Cambodia"
msgstr "Καμπότζη"

#: ../../english/template/debian/countries.wml:273
msgid "Korea"
msgstr "Κορέα"

#: ../../english/template/debian/countries.wml:276
msgid "Kuwait"
msgstr "Κουβέιτ"

#: ../../english/template/debian/countries.wml:279
msgid "Kazakhstan"
msgstr "Καζακστάν"

#: ../../english/template/debian/countries.wml:282
msgid "Sri Lanka"
msgstr "Σρι-Λάνκα"

#: ../../english/template/debian/countries.wml:285
msgid "Lithuania"
msgstr "Λιθουανία"

#: ../../english/template/debian/countries.wml:288
msgid "Luxembourg"
msgstr "Λουξεμβούργο"

#: ../../english/template/debian/countries.wml:291
msgid "Latvia"
msgstr "Λετονία"

#: ../../english/template/debian/countries.wml:294
msgid "Morocco"
msgstr "Μαρόκο"

#: ../../english/template/debian/countries.wml:297
msgid "Moldova"
msgstr "Μολδοβία"

#: ../../english/template/debian/countries.wml:300
msgid "Montenegro"
msgstr "Μαυροβούνιο"

#: ../../english/template/debian/countries.wml:303
msgid "Madagascar"
msgstr "Μαδαγασκάρη"

#: ../../english/template/debian/countries.wml:306
msgid "Macedonia, Republic of"
msgstr "Μακεδονία, Δημοκρατία της"

#: ../../english/template/debian/countries.wml:309
msgid "Mongolia"
msgstr "Μογγολία"

#: ../../english/template/debian/countries.wml:312
msgid "Malta"
msgstr "Μάλτα"

#: ../../english/template/debian/countries.wml:315
msgid "Mexico"
msgstr "Μεξικό"

#: ../../english/template/debian/countries.wml:318
msgid "Malaysia"
msgstr "Μαλαισία"

#: ../../english/template/debian/countries.wml:321
msgid "New Caledonia"
msgstr "Νέα Καληδονία"

#: ../../english/template/debian/countries.wml:324
msgid "Nicaragua"
msgstr "Νικαράγουα"

#: ../../english/template/debian/countries.wml:327
msgid "Netherlands"
msgstr "Ολλανδία"

#: ../../english/template/debian/countries.wml:330
msgid "Norway"
msgstr "Νορβηγία"

#: ../../english/template/debian/countries.wml:333
msgid "New Zealand"
msgstr "Νέα Ζηλανδία"

#: ../../english/template/debian/countries.wml:336
msgid "Panama"
msgstr "Παναμάς"

#: ../../english/template/debian/countries.wml:339
msgid "Peru"
msgstr "Περού"

#: ../../english/template/debian/countries.wml:342
msgid "French Polynesia"
msgstr "Γαλλική Πολυνησία"

#: ../../english/template/debian/countries.wml:345
msgid "Philippines"
msgstr "Φιλιππίνες"

#: ../../english/template/debian/countries.wml:348
msgid "Pakistan"
msgstr "Πακιστάν"

#: ../../english/template/debian/countries.wml:351
msgid "Poland"
msgstr "Πολωνία"

#: ../../english/template/debian/countries.wml:354
msgid "Portugal"
msgstr "Πορτογαλία"

#: ../../english/template/debian/countries.wml:357
msgid "Réunion"
msgstr "Réunion"

#: ../../english/template/debian/countries.wml:360
msgid "Romania"
msgstr "Ρουμανία"

#: ../../english/template/debian/countries.wml:363
msgid "Serbia"
msgstr "Σερβία"

#: ../../english/template/debian/countries.wml:366
msgid "Russia"
msgstr "Ρωσία"

#: ../../english/template/debian/countries.wml:369
msgid "Saudi Arabia"
msgstr "Σαουδική Αραβία"

#: ../../english/template/debian/countries.wml:372
msgid "Sweden"
msgstr "Σουηδία"

#: ../../english/template/debian/countries.wml:375
msgid "Singapore"
msgstr "Σιγκαπούρη"

#: ../../english/template/debian/countries.wml:378
msgid "Slovenia"
msgstr "Σλοβενία"

#: ../../english/template/debian/countries.wml:381
msgid "Slovakia"
msgstr "Σλοβακία"

#: ../../english/template/debian/countries.wml:384
msgid "El Salvador"
msgstr "Ελ Σαλβαδόρ"

#: ../../english/template/debian/countries.wml:387
msgid "Thailand"
msgstr "Ταϊλάνδη"

#: ../../english/template/debian/countries.wml:390
msgid "Tajikistan"
msgstr "Τατζικιστάν"

#: ../../english/template/debian/countries.wml:393
msgid "Tunisia"
msgstr "Τυνισία"

#: ../../english/template/debian/countries.wml:396
msgid "Turkey"
msgstr "Τουρκία"

#: ../../english/template/debian/countries.wml:399
msgid "Taiwan"
msgstr "Ταΐβάν"

#: ../../english/template/debian/countries.wml:402
msgid "Ukraine"
msgstr "Ουκρανία"

#: ../../english/template/debian/countries.wml:405
msgid "United States"
msgstr "Ην. Πολιτείες"

#: ../../english/template/debian/countries.wml:408
msgid "Uruguay"
msgstr "Ουρουγουάη"

#: ../../english/template/debian/countries.wml:411
msgid "Uzbekistan"
msgstr "Ουζμπεκιστάν"

#: ../../english/template/debian/countries.wml:414
msgid "Venezuela"
msgstr "Βενεζουέλα"

#: ../../english/template/debian/countries.wml:417
msgid "Vietnam"
msgstr "Βιετνάμ"

#: ../../english/template/debian/countries.wml:420
msgid "Vanuatu"
msgstr "Βανουάτου"

#: ../../english/template/debian/countries.wml:423
msgid "South Africa"
msgstr "Νότια Αφρική"

#: ../../english/template/debian/countries.wml:426
msgid "Zimbabwe"
msgstr "Ζιμπάμπουε"

#~ msgid "Great Britain"
#~ msgstr "Μ. Βρετανία"
