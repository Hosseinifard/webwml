# Templates files for stats modules
# Copyright (C) 2011 Software in the Public Interest, Inc.
#
# David Prévot <david@tilapin.org>, 2011, 2012.
# Jean-Paul Guillonneau <guillonneau.jeanpaul@free.fichier>, 2017-18.
msgid ""
msgstr ""
"Project-Id-Version: \n"
"PO-Revision-Date: 2018-06-24 08:58+0200\n"
"Last-Translator: Jean-Paul Guillonneau <guillonneau.jeanpaul@free.fr>\n"
"Language-Team: French <debian-l10n-french@lists.debian.org>\n"
"Language: fr\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: Lokalize 1.2\n"
"Plural-Forms: nplurals=2; plural=(n > 1);\n"

#: ../../english/template/debian/stats_tags.wml:6
msgid "Debian web site translation statistics"
msgstr "Statistiques de traduction du site web Debian"

#: ../../english/template/debian/stats_tags.wml:10
msgid "There are %d pages to translate."
msgstr "%d pages sont à traduire."

#: ../../english/template/debian/stats_tags.wml:14
msgid "There are %d bytes to translate."
msgstr "%d octets sont à traduire."

#: ../../english/template/debian/stats_tags.wml:18
msgid "There are %d strings to translate."
msgstr "%d chaînes sont à traduire."

#: ../../stattrans.pl:282 ../../stattrans.pl:498
msgid "Wrong translation version"
msgstr "Mauvaise version de traduction"

#: ../../stattrans.pl:284
msgid "This translation is too out of date"
msgstr "Cette traduction est trop incomplète"

#: ../../stattrans.pl:286
msgid "The original is newer than this translation"
msgstr "L'original est plus récent que sa traduction"

#: ../../stattrans.pl:290 ../../stattrans.pl:498
msgid "The original no longer exists"
msgstr "L'original n'existe plus"

#: ../../stattrans.pl:474
msgid "hit count N/A"
msgstr "Nombre de visites indisponible"

#: ../../stattrans.pl:474
msgid "hits"
msgstr "visites"

#: ../../stattrans.pl:492 ../../stattrans.pl:493
msgid "Click to fetch diffstat data"
msgstr "Cliquer pour récupérer les statistiques du différentiel"

#: ../../stattrans.pl:603 ../../stattrans.pl:743
msgid "Created with <transstatslink>"
msgstr "Créé avec <transstatslink>"

#: ../../stattrans.pl:608
msgid "Translation summary for"
msgstr "Vue globale des traductions pour la langue :"

#: ../../stattrans.pl:611 ../../stattrans.pl:767 ../../stattrans.pl:813
#: ../../stattrans.pl:856
msgid "Not translated"
msgstr "Non traduites"

#: ../../stattrans.pl:611 ../../stattrans.pl:766 ../../stattrans.pl:812
msgid "Outdated"
msgstr "Incomplètes"

#: ../../stattrans.pl:611
msgid "Translated"
msgstr "Traduites"

#: ../../stattrans.pl:611 ../../stattrans.pl:691 ../../stattrans.pl:765
#: ../../stattrans.pl:811 ../../stattrans.pl:854
msgid "Up to date"
msgstr "À jour"

#: ../../stattrans.pl:612 ../../stattrans.pl:613 ../../stattrans.pl:614
#: ../../stattrans.pl:615
msgid "files"
msgstr "fichiers"

#: ../../stattrans.pl:618 ../../stattrans.pl:619 ../../stattrans.pl:620
#: ../../stattrans.pl:621
msgid "bytes"
msgstr "octets"

#: ../../stattrans.pl:628
msgid ""
"Note: the lists of pages are sorted by popularity. Hover over the page name "
"to see the number of hits."
msgstr ""
"Remarque : les listes de pages sont classées par popularité. Le nombre de "
"visites apparaît lorsque le pointeur passe sur le nom de la page."

#: ../../stattrans.pl:634
msgid "Outdated translations"
msgstr "Traductions incomplètes"

#: ../../stattrans.pl:636 ../../stattrans.pl:690
msgid "File"
msgstr "Fichier"

#: ../../stattrans.pl:638
msgid "Diff"
msgstr "Différentiel"

#: ../../stattrans.pl:640
msgid "Comment"
msgstr "Commentaire"

#: ../../stattrans.pl:641
msgid "Diffstat"
msgstr "Statistiques du différentiel"

#: ../../stattrans.pl:642
msgid "Git command line"
msgstr "Ligne de commande de Git"

#: ../../stattrans.pl:644
msgid "Log"
msgstr "Journal"

#: ../../stattrans.pl:645
msgid "Translation"
msgstr "Traduction"

#: ../../stattrans.pl:646
msgid "Maintainer"
msgstr "Responsable"

#: ../../stattrans.pl:648
msgid "Status"
msgstr "État"

#: ../../stattrans.pl:649
msgid "Translator"
msgstr "Traducteur"

#: ../../stattrans.pl:650
msgid "Date"
msgstr "Date"

#: ../../stattrans.pl:657
msgid "General pages not translated"
msgstr "Pages générales non traduites"

#: ../../stattrans.pl:658
msgid "Untranslated general pages"
msgstr "Pages générales non traduites"

#: ../../stattrans.pl:663
msgid "News items not translated"
msgstr "Nouvelles non traduites"

#: ../../stattrans.pl:664
msgid "Untranslated news items"
msgstr "Nouvelles non traduites"

#: ../../stattrans.pl:669
msgid "Consultant/user pages not translated"
msgstr "Pages de consultants et d'utilisateurs non traduites"

#: ../../stattrans.pl:670
msgid "Untranslated consultant/user pages"
msgstr "Pages de consultants et d'utilisateurs non traduites"

#: ../../stattrans.pl:675
msgid "International pages not translated"
msgstr "Pages internationales non traduites"

#: ../../stattrans.pl:676
msgid "Untranslated international pages"
msgstr "Pages internationales non traduites"

#: ../../stattrans.pl:681
msgid "Translated pages (up-to-date)"
msgstr "Pages web traduites (à jour)"

#: ../../stattrans.pl:688 ../../stattrans.pl:838
msgid "Translated templates (PO files)"
msgstr "Chaînes traduites (fichiers PO)"

#: ../../stattrans.pl:689 ../../stattrans.pl:841
msgid "PO Translation Statistics"
msgstr "Statistiques de traduction des fichiers PO"

#: ../../stattrans.pl:692 ../../stattrans.pl:855
msgid "Fuzzy"
msgstr "Approximatives"

#: ../../stattrans.pl:693
msgid "Untranslated"
msgstr "Non traduites"

#: ../../stattrans.pl:694
msgid "Total"
msgstr "Total"

#: ../../stattrans.pl:711
msgid "Total:"
msgstr "Total :"

#: ../../stattrans.pl:745
msgid "Translated web pages"
msgstr "Pages web traduites"

#: ../../stattrans.pl:748
msgid "Translation Statistics by Page Count"
msgstr "Statistiques de traduction par nombre de pages"

#: ../../stattrans.pl:763 ../../stattrans.pl:809 ../../stattrans.pl:853
msgid "Language"
msgstr "Langue"

#: ../../stattrans.pl:764 ../../stattrans.pl:810
msgid "Translations"
msgstr "Traductions"

#: ../../stattrans.pl:791
msgid "Translated web pages (by size)"
msgstr "Pages web traduites (par taille)"

#: ../../stattrans.pl:794
msgid "Translation Statistics by Page Size"
msgstr "Statistiques de traduction par taille de pages"

#~ msgid "Unified diff"
#~ msgstr "Différentiel unifié"

#~ msgid "Colored diff"
#~ msgstr "Différentiel coloré"

#~| msgid "Colored diff"
#~ msgid "Commit diff"
#~ msgstr "Différentiel de commit"

#~ msgid "Created with"
#~ msgstr "Page créée avec"

#~ msgid "Origin"
#~ msgstr "Original"
