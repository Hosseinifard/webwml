#use wml::debian::translation-check translation="1d1c1ba842e225bf68a6fed5744786cc779234f7" maintainer="Jean-Pierre Giraud"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>
<p>Jonas Klempel a découvert qu'Apache Tomcat Native, lors de l'analyse du
champ AIA-Extension d'un certificat de client, ne gérait pas correctement
les champs plus longs que 127 octets. La conséquence de l'erreur d'analyse
était l'omission de la vérification OCSP. Il était donc possible que soient
acceptés des certificats de clients qui auraient été rejetés si la
vérification OCSP avait été réalisée. Les utilisateurs n'effectuant pas
les vérifications OCSP ne sont pas affectés par cette vulnérabilité.</p>

<p>Pour Debian 7 <q>Wheezy</q>, ces problèmes ont été corrigés dans la
version 1.1.24-1+deb7u1.</p>

<p>Nous vous recommandons de mettre à jour vos paquets tomcat-native.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS,
comment appliquer ces mises à jour dans votre système et les questions
fréquemment posées peuvent être trouvées sur :
<a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2018/dla-1276.data"
# $Id: $
