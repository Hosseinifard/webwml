#use wml::debian::translation-check translation="ce41d997301872adfc27a79ea546429856226b67" maintainer="Jean-Pierre Giraud"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>

<p>Trois problèmes de sécurité ont été découverts dans cacti :</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2016-2313">CVE-2016-2313</a>

<p>auth_login.php permet à des utilisateurs distants authentifiés qui
utilisent l'authentification web de contourner des restrictions d'accès
voulues en se connectant comme un utilisateur absent de la base de données
de cacti.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2016-3172">CVE-2016-3172</a>

<p>Une vulnérabilité d'injection SQL dans tree.php permet à des
utilisateurs distants authentifiés d'exécuter des commandes SQL arbitraires
à l'aide du paramètre parent_id dans une action item_edit.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2016-3659">CVE-2016-3659</a>

<p>Une vulnérabilité d'injection SQL dans graph_view.php permet à des
utilisateurs distants authentifiés d'exécuter des commandes SQL arbitraires
à l'aide du paramètre host_group_data.</p></li>

</ul>

<p>Pour Debian 7 <q>Wheezy</q>, ces problèmes ont été corrigés dans la
version 0.8.8a+dfsg-5+deb7u9.</p>

<p>Nous vous recommandons de mettre à jour vos paquets cacti.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS,
comment appliquer ces mises à jour dans votre système et les questions
fréquemment posées peuvent être trouvées sur :
<a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2016/dla-560.data"
# $Id: $
