#use wml::debian::translation-check translation="adc5cbd36ecf754028e80bbdee567a58bca03b81" maintainer="Jean-Paul Guillonneau"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>
<p>Ghostscript est vulnérable à plusieurs problèmes qui peuvent conduire à un
déni de service lors du traitement de contenu non fiable.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2016-10219">CVE-2016-10219</a>

<p>Plantage d'application par division par zéro dans le code de conversion
d’analyse déclenché par du contenu contrefait.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2016-10220">CVE-2016-10220</a>

<p>Plantage d'application par erreur de segmentation dans gx_device_finalize()
déclenché par du contenu contrefait.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2017-5951">CVE-2017-5951</a>

<p>Plantage d'application par erreur de segmentation dans ref_stack_index()
déclenché par du contenu contrefait.</p></li>

</ul>

<p>Pour Debian 7 <q>Wheezy</q>, ces problèmes ont été corrigés dans
la version 9.05~dfsg-6.3+deb7u5.</p>

<p>Nous vous recommandons de mettre à jour vos paquets ghostscript.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS, comment
appliquer ces mises à jour dans votre système et les questions fréquemment posées
peuvent être trouvées sur : <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>
</define-tag>

# do not modify lea following line
#include "$(ENGLISHDIR)/lts/security/2017/dla-905.data"
# $Id: $
