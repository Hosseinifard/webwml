<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>It was discovered that tmpreaper, a program that cleans up files in
directories based on their age, is vulnerable to a race condition. This
vulnerability might be exploited by local attackers to perform privilege
escalation.</p>

<p>For Debian 8 <q>Jessie</q>, this problem has been fixed in version
1.6.13+nmu1+deb8u1.</p>

<p>We recommend that you upgrade your tmpreaper packages.</p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2019/dla-1640.data"
# $Id: $
