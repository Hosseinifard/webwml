<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>Two security vulnerabilities were found in OpenSSL, the Secure Sockets
Layer toolkit.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-1547">CVE-2019-1547</a>

    <p>Normally in OpenSSL EC groups always have a co-factor present and
    this is used in side channel resistant code paths. However, in some
    cases, it is possible to construct a group using explicit parameters
    (instead of using a named curve). In those cases it is possible that
    such a group does not have the cofactor present. This can occur even
    where all the parameters match a known named curve. If such a curve
    is used then OpenSSL falls back to non-side channel resistant code
    paths which may result in full key recovery during an ECDSA
    signature operation. In order to be vulnerable an attacker
    would have to have the ability to time the creation of a large
    number of signatures where explicit parameters with no co-factor
    present are in use by an application using libcrypto. For the
    avoidance of doubt libssl is not vulnerable because explicit
    parameters are never used.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-1563">CVE-2019-1563</a>

    <p>In situations where an attacker receives automated notification of
    the success or failure of a decryption attempt an attacker, after
    sending a very large number of messages to be decrypted, can recover
    a CMS/PKCS7 transported encryption key or decrypt any RSA encrypted
    message that was encrypted with the public RSA key, using a
    Bleichenbacher padding oracle attack. Applications are not affected
    if they use a certificate together with the private RSA key to the
    CMS_decrypt or PKCS7_decrypt functions to select the correct
    recipient info to decrypt.</p></li>

</ul>

<p>For Debian 8 <q>Jessie</q>, these problems have been fixed in version
1.0.1t-1+deb8u12.</p>

<p>We recommend that you upgrade your openssl packages.</p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a rel="nofollow" href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2019/dla-1932.data"
# $Id: $
