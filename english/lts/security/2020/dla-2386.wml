<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>Several vulnerabilities were discovered in the Perl5 Database
Interface (DBI). An attacker could trigger a denial-of-service (DoS)
and possibly execute arbitrary code.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-20919">CVE-2019-20919</a>

    <p>The hv_fetch() documentation requires checking for NULL and the
    code does that. But, shortly thereafter, it calls SvOK(profile),
    causing a NULL pointer dereference.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-14392">CVE-2020-14392</a>

    <p>An untrusted pointer dereference flaw was found in Perl-DBI. A
    local attacker who is able to manipulate calls to
    dbd_db_login6_sv() could cause memory corruption, affecting the
    service's availability.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-14393">CVE-2020-14393</a>

    <p>A buffer overflow on via an overlong DBD class name in
    dbih_setup_handle function may lead to data be written past the
    intended limit.</p></li>

</ul>

<p>For Debian 9 stretch, these problems have been fixed in version
1.636-1+deb9u1.</p>

<p>We recommend that you upgrade your libdbi-perl packages.</p>

<p>For the detailed security status of libdbi-perl please refer to
its security tracker page at:
<a href="https://security-tracker.debian.org/tracker/libdbi-perl">https://security-tracker.debian.org/tracker/libdbi-perl</a></p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2020/dla-2386.data"
# $Id: $
