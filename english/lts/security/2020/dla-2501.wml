<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>

<p>An issue has been found in influxdb, a scalable datastore for metrics,
events, and real-time analytics.
By using a JWT token with an empty shared secret, one is able to bypass
authentication in services/httpd/handler.go.</p>


<p>For Debian 9 stretch, this problem has been fixed in version
1.1.1+dfsg1-4+deb9u1.</p>

<p>We recommend that you upgrade your influxdb packages.</p>

<p>For the detailed security status of influxdb please refer to
its security tracker page at:
<a href="https://security-tracker.debian.org/tracker/influxdb">https://security-tracker.debian.org/tracker/influxdb</a></p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2020/dla-2501.data"
# $Id: $
