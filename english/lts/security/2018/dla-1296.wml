<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>Kelby Ludwig and Scott Cantor discovered that the Shibboleth service
provider is vulnerable to impersonation attacks and information
disclosure due to incorrect XML parsing. For additional details please
refer to the upstream advisory at
<a href="https://shibboleth.net/community/advisories/secadv_20180227.txt">https://shibboleth.net/community/advisories/secadv_20180227.txt</a></p>

<p>For Debian 7 <q>Wheezy</q>, these problems have been fixed in version
1.4.2-5+deb7u3.</p>

<p>We recommend that you upgrade your xmltooling packages.</p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2018/dla-1296.data"
# $Id: $
