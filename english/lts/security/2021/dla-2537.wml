<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>Two vulnerabilities have been discovered in ffmpeg, a widely used
multimedia framework.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-17539">CVE-2019-17539</a>

    <p>a NULL pointer dereference and possibly unspecified other impact
    when there is no valid close function pointer</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-35965">CVE-2020-35965</a>

    <p>an out-of-bounds write because of errors in calculations of when to
    perform memset zero operations</p></li>

</ul>

<p>For Debian 9 stretch, these problems have been fixed in version
7:3.2.15-0+deb9u2.</p>

<p>We recommend that you upgrade your ffmpeg packages.</p>

<p>For the detailed security status of ffmpeg please refer to
its security tracker page at:
<a href="https://security-tracker.debian.org/tracker/ffmpeg">https://security-tracker.debian.org/tracker/ffmpeg</a></p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2021/dla-2537.data"
# $Id: $
