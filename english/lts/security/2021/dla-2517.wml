<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>

<p>It was discovered that there were two issues in the Dovecot IMAP server:</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-24386">CVE-2020-24386</a>

    <p>An issue was discovered in Dovecot before 2.3.13. By using IMAP IDLE, an
    authenticated attacker can trigger unhibernation via attacker-controlled
    parameters, leading to access to other users' email messages (and path
    disclosure).</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-25275">CVE-2020-25275</a>

    <p>Dovecot before 2.3.13 has Improper Input Validation in lda, lmtp, and
    imap, leading to an application crash via a crafted email message with
    certain choices for ten thousand MIME parts.</p></li>

</ul>

<p>For Debian 9 <q>Stretch</q>, these problems have been fixed in version
1:2.2.27-3+deb9u7.</p>

<p>We recommend that you upgrade your dovecot packages.</p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2021/dla-2517.data"
# $Id: $
